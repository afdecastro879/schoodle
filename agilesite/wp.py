from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse
from django.conf import settings

from wpadmin.utils import get_admin_site_name
from wpadmin.menu import items
from wpadmin.menu.menus import Menu


class UserTopMenu(Menu):

    def my_user_check(self, user):
        """
        Custom helper method for hiding some menu items from not allowed users.
        """
        return user.groups.filter(name='users').exists()

    def init_with_context(self, context):

        self.children += [
            items.MenuItem(
                title='',
                url="/",
                icon='noicon',
                css_styles='font-size: 1.5em;',
            ),
        ]

        self.children += [
            items.UserTools(
                css_styles='float: right;',
            ),
        ]