from django.conf.urls import patterns, include, url
from django.conf.urls.static import static

from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from settings import common

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'coordinador.views.index', name='home'),
    url(r'^session/', 'coordinador.views.session', name='plan'),
    url(r'^plan$', 'coordinador.views.planeador', name='planeador'),
    url(r'^folder$', 'coordinador.views.carpeta', name='carpeta'),
    url(r'^perfil$', 'coordinador.views.perfil', name='perfil'),
    url(r'^agregarRegistro/(?P<subjectId>[0-9]+)-(?P<semesterId>[0-9]+)$', 'coordinador.views.agregarRegistro', name='agregarRegistro'),
    url(r'^deleteRegistro/(?P<idRegistro>[0-9]+)$', 'coordinador.views.deleteRegistro', name='deleteRegistro'),
    url(r'^infoMateria/(?P<id>[0-9]+)$', 'coordinador.views.getInfoMateria', name='infoMateria'),
    url(r'^getColor/(?P<aprobados>[0-9]+)-(?P<creditosMinimos>[0-9]+)$', 'coordinador.views.getColor', name='color'),
    url(r'^materias/(?P<maestria>[0-9]+)$', 'coordinador.views.materiasSinPlanearPorMaestria', name='materias'),
    url(r'^optimizador/', 'coordinador.views.optimizador', name='optimizador'),
    url(r'^optimizadorExample/', 'coordinador.views.optimizadorExample', name='example'),
    url(r'^optimizadorExecute/', 'coordinador.views.optimizadorExecute', name='execute'),
    url(r'^demandaMaterias/(?P<semestre>[0-9]+)$', 'coordinador.views.demandaMateriasPorMaestria', name='demandaMaterias'),
    url(r'^semestre$', 'coordinador.views.semestres', name='semestres'),
    url(r'^crearDemanda/', 'coordinador.views.crearDemanda', name='crearDemanda'),
    url(r'^crearHistorico/', 'coordinador.views.crearHistorico', name='crearHistorico'),
    url(r'^getConfiguracion/', 'coordinador.views.getConfiguracion', name='configurador'),
    url(r'^guardarConfiguracion$', 'coordinador.views.guardarConfiguracion', name='guardar'),
    url(r'^login/', 'coordinador.views.schoodle_login', name='login'),
    url(r'^logout/', 'coordinador.views.schoodle_logout', name='logout'),
    url(r'^estudiantes/', 'coordinador.views.getListaEstudiantes', name='estudiantes'),
    url(r'^estudiante/(?P<idestudiante>[0-9]+)$', 'coordinador.views.obtenerDatosEstudiante', name='estudiante'),
)
urlpatterns += patterns('',
    (r"^static/(.*)$", 'django.views.static.serve', {'document_root': common.STATIC_ROOT}),
)
urlpatterns += patterns('',
    url(r'^jsreverse/$', 'django_js_reverse.views.urls_js', name='js_reverse'),
)
urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(common.MEDIA_URL, document_root=common.MEDIA_ROOT)