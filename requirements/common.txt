Django == 1.6.5
psycopg2
dj-database-url
dj-static
South
django-bootstrap3
Pillow
django-fontawesome
django-compressor
django-js-reverse
django-wpadmin