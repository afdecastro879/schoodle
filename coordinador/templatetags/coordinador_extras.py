from django import template

register = template.Library()

@register.simple_tag
def active(request, pattern):
    if request.path == pattern:
        return 'active'
    return ''

@register.simple_tag
def subjectColor(aprobadosUser, creditosMinimos):
    if aprobadosUser >= creditosMinimos:
        return "green"
    elif aprobadosUser+10 >= creditosMinimos:
        return "yellow"
    elif aprobadosUser+20 >= creditosMinimos:
        return "orange"
    else:
        return "red"

@register.simple_tag
def subjectClassColor(creditosMinimos):
    if creditosMinimos <=0:
        return "green_class"
    elif creditosMinimos > 0 and creditosMinimos <=8:
        return "yellow_class"
    elif creditosMinimos > 8 and creditosMinimos <=16:
        return "orange_class"
    else:
        return "red_class"

@register.simple_tag
def color(efectividad):
    if efectividad>80:
        return "green"
    elif efectividad>60:
        return "yellow"
    elif efectividad>30:
        return "orange"
    else:
        return "red"