# -*- coding: utf-8 -*-
# Create your views here.
import random
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import user_passes_test, login_required
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from django.http import HttpResponse
from coordinador.models import Programa, TipoPrograma, Estudiante, ProgramaEstudiante, Semestre, RegistroCurso, Curso, \
    Coordinador, PorcentajePrograma, ConfiguracionCurso, \
    TipoRegistroCurso
import json
import datetime
from django.db.models import Count


def estudiante_check(user):
    try:
        user.estudiante
        return True
    except ObjectDoesNotExist:
        return False
    except AttributeError:
        return False


def coordinador_check(user):
    try:
        user.coordinador
        return True
    except ObjectDoesNotExist:
        return False
    except AttributeError:
        return False


@user_passes_test(estudiante_check, login_url='/login/')
def index(request):
    # print request.session['user']
    return render_to_response('index.html', {}, context_instance=RequestContext(request))


def session(request):
    estudiantes = Estudiante.objects.all()
    if len(estudiantes) > 0:
        sesion = 0
        find = False
        for estudiante in estudiantes:
            if estudiante.id > sesion:
                find = True
                request.session['user'] = estudiante.id
                data = {'name': estudiante.nombre}
        if find == False:
            primero = estudiantes[0]
            request.session['user'] = primero.id
            data = {'name': primero.nombre}
    else:
        data = {'name': 'No users'}

    return HttpResponse(json.dumps(data), content_type='application/json; charset=UTF-8')


@user_passes_test(estudiante_check, login_url='/login/')
def planeador(request):
    # Aca debe cargar toda la info del usuario
    user = request.user.estudiante
    nombreUsuario = request.user.first_name
    # Cargamos las maestrias
    tipo_maestria = TipoPrograma.objects.get(nombre='MAESTRIA')
    prog_est = ProgramaEstudiante.objects.filter(actual=1).get(estudiante=user.id)
    lista_maestrias = Programa.objects.filter(tipo_programa=tipo_maestria).exclude(pk=prog_est.programa.pk)
    maestrias = []
    for maes in lista_maestrias:
        temp = {
            'id': maes.id,
            'codigo': maes.codigo,
            'nombre': maes.nombre,
            'descripcion': maes.descripcion
        }
        maestrias.append(temp)

    # Cargamos la maestria del estudiante
    maestria = {}
    if prog_est.programa.tipo_programa.pk == tipo_maestria.pk:
        maestria = {'nombre': prog_est.programa.nombre, 'descripcion': prog_est.programa.descripcion,
                    'codigo': prog_est.programa.codigo, 'id': prog_est.programa.id,
                    'cursos': user.getCursosSinPlanear(prog_est.programa.id)}
    else:
        maestria = None

    data = {'maestria': maestria,
            'lista_maestrias': maestrias,
            'estudiante': user.json_plan(),
            'nombreUsuario': nombreUsuario}

    return render_to_response('planeador.html', data, context_instance=RequestContext(request))


@user_passes_test(estudiante_check, login_url='/login/')
def carpeta(request):
    return render_to_response('carpeta.html', {}, context_instance=RequestContext(request))


@user_passes_test(estudiante_check, login_url='/login/')
def perfil(request):
    return render_to_response('perfil.html', {}, context_instance=RequestContext(request))


@user_passes_test(estudiante_check, login_url='/login/')
def deleteRegistro(request, idRegistro):
    user = request.user.estudiante
    r = RegistroCurso.objects.get(id=idRegistro)
    estudiante = r.estudiante
    if estudiante.id == user.id and r.tipo_registro.nombre == "PLANEADO":
        r.delete()
        html = {'response': 'success', 'data': user.json_plan()}
        return HttpResponse(json.dumps(html), content_type='application/json; charset=UTF-8')
    else:
        html = {'response': 'failed'}
        return HttpResponse(json.dumps(html), content_type='application/json; charset=UTF-8')


@user_passes_test(estudiante_check, login_url='/login/')
def getColor(request, aprobados, creditosMinimos):
    if aprobados >= creditosMinimos:
        html = {'response': 'success', 'color': 'green'}
    elif aprobados + 8 >= creditosMinimos:
        html = {'response': 'success', 'color': 'yellow'}
    elif aprobados + 16 >= creditosMinimos:
        html = {'response': 'success', 'color': 'orange'}
    else:
        html = {'response': 'success', 'color': 'red'}
    return HttpResponse(json.dumps(html), content_type='application/json; charset=UTF-8')


@user_passes_test(estudiante_check, login_url='/login/')
def agregarRegistro(request, subjectId, semesterId):
    user = request.user.estudiante
    estudiante = Estudiante.objects.get(id=user.id)
    semestre = Semestre.objects.get(id=semesterId)
    curso = Curso.objects.get(id=subjectId)
    registro = RegistroCurso.objects.filter(semestre=semestre, estudiante=estudiante, curso=curso)
    if registro:
        html = {'response': 'failed'}
    else:
        tipo_reg = TipoRegistroCurso.objects.get(id=3)
        r = RegistroCurso.objects.create(semestre=semestre, estudiante=estudiante,
                                         curso=curso, tipo_registro=tipo_reg)
        r.save()
        html = {'response': 'success', 'data': user.json_plan()}
    return HttpResponse(json.dumps(html), content_type='application/json; charset=UTF-8')


@user_passes_test(estudiante_check, login_url='/login/')
def materiasSinPlanearPorMaestria(request, maestria):
    user = request.user.estudiante
    return HttpResponse(json.dumps(user.getCursosSinPlanear(maestria)), content_type='application/json; charset=UTF-8')


@user_passes_test(estudiante_check, login_url='/login/')
def getInfoMateria(request, id):
    materia = Curso.objects.get(pk=id)
    if materia:
        data = materia.json()
    else:
        data = {}
    return HttpResponse(json.dumps(data), content_type='application/json; charset=UTF-8')


@user_passes_test(coordinador_check, login_url='/login/')
def demandaMateriasPorMaestria(request, semestre):
    # Session del coordinador
    coordinador = request.user.coordinador
    program = Programa.objects.filter(coordinador=coordinador.id)
    program = program[0]

    if semestre == "0":
        semestre = Semestre.objects.filter(fecha_inicial__gte=datetime.datetime.now()).order_by('fecha_inicial')
        semestre = semestre[0].id

    demanda_cursos = RegistroCurso.objects.all().filter(curso__programa__id=program.id, semestre__id=semestre)

    ids = demanda_cursos.values('curso__id')
    demanda_cursos = demanda_cursos.values('curso__nombre', 'curso__codigo').annotate(
        num_estudiantes=Count('curso__id'))
    demanda_cursos = demanda_cursos.order_by('-num_estudiantes')

    data = []

    for curso in demanda_cursos:
        record = {
            'num_estudiantes': curso['num_estudiantes'],
            'curso': curso['curso__nombre'],
            'codigo': curso['curso__codigo']
        }
        data.append(record)
    faltantes = Curso.objects.filter(programa__id=program.id).exclude(id__in=ids).all()
    for curso in faltantes:
        record = {
            'num_estudiantes': 0,
            'curso': curso.nombre,
            'codigo': curso.codigo,
        }
        data.append(record)

    return HttpResponse(json.dumps(data), content_type='application/json; charset=UTF-8')


@login_required(login_url='/login/')
def semestres(request):
    semestres_data = Semestre.objects.all()
    semestres_data = [semestre.json() for semestre in semestres_data]
    return HttpResponse(json.dumps(semestres_data), content_type='application/json; charset=UTF-8')


@user_passes_test(coordinador_check, login_url='/login/')
def optimizador(request):
    coordinador = request.user.coordinador
    nombreCoordinador = request.user.coordinador.usuario.first_name
    programa = Programa.objects.filter(coordinador=coordinador.pk).get()
    return render_to_response('optimizador.html',
                              {'programa': programa.descripcion, 'nombreCoordinador': nombreCoordinador},
                              context_instance=RequestContext(request))
    """# Sacamos los periodos
    import time

    current_year = time.strftime("%Y")
    current_month = time.strftime("%m")
    if current_month <= 4:
         future_semesters = [current_year + "-18", current_year + "-20",
         str(int(current_year) + 1) + "-10", str(int(current_year) + 1) + "-18"]
    elif current_month in [6, 5]:
         future_semesters = [current_year + "-20", str(int(current_year) + 1) + "-01",
         str(int(current_year) + 1) + "-18", str(int(current_year) + 1) + "-20"]
    else:
         future_semesters = [str(int(current_year) + 1) + "-10", str(int(current_year) + 1) + "-18",
         str(int(current_year) + 1) + "-20", str(int(current_year) + 2) + "-10"]
         list_semesters = Semestre.objects.filter(planeador_estudio=1, estado=True,
                                                     nombre__in=future_semesters).all()
         #Saco todos los registros realizados de todos los estudiantes
         list_registers = RegistroCurso.objects.filter(semestre__in=list_semesters,
                                                          tipo_registro="3").all()
    # creditos presentes en el planeador
    creditos_totales_planeador = 0
    plan_student = []
    for semester in list_semesters:
         lista_materias = []
         for register in list_registers:
             # print register
            if register.semestre == semester:
               lista_materias.append({'curso': register.curso, 'id': register.id})
               creditos_totales_planeador += register.curso.creditos
         materias_semestre = {'name': semester.nombre, 'data': lista_materias, 'id': semester.id}
"""


@user_passes_test(coordinador_check, login_url='/login/')
def optimizadorExecute(request):
    data = {}
    # Session del coordinador
    coordinador = request.user.coordinador
    # Conseguir programa del coordinador
    programa = Programa.objects.filter(coordinador=coordinador.pk).get()
    # Conseguir periodo siguiente
    periodo = \
        Semestre.objects.filter(planeador_estudio=1, estado=True,
                                fecha_inicial__gte=datetime.datetime.now()).order_by(
            'fecha_inicial')[0]
    # Traer los registros del periodo siguiente de cursos que pertenecen al programa
    cursos = Curso.objects.filter(programa=programa.pk).all()
    registros = RegistroCurso.objects.filter(curso__in=cursos, semestre=periodo.pk,
                                             tipo_registro__nombre='PLANEADO').order_by('curso').all()
    # Creamos mapa de materias
    map = {}
    for registro in registros:
        if map.get(registro.curso.nombre):
            map[registro.curso.nombre].append(registro)
        else:
            map[registro.curso.nombre] = []
            map[registro.curso.nombre].append(registro)

    # Traer la configuracion
    configuraciones = ConfiguracionCurso.objects.filter(semestre=periodo.pk, curso__in=cursos).all()
    materias = []
    grupos = {'programas': [], 'niveles': []}
    programaGeneralMap = {}
    nivelesGeneralMap = {'32-40': {'nombre': '32-40', 'total': 0, 'asignados': 0, 'no_asignados': 0},
                         '24-32': {'nombre': '24-32', 'total': 0, 'asignados': 0, 'no_asignados': 0},
                         '16-24': {'nombre': '16-24', 'total': 0, 'asignados': 0, 'no_asignados': 0},
                         '8-16': {'nombre': '8-16', 'total': 0, 'asignados': 0, 'no_asignados': 0},
                         '0-8': {'nombre': '0-8', 'total': 0, 'asignados': 0, 'no_asignados': 0}}
    asignados = 0
    totalGeneral = 0
    for config in configuraciones:
        materia = {}
        # Tomo el grupo de registros que pertenecen a un curso
        registrosConfig = map.get(config.curso.nombre)
        if registrosConfig == None: registrosConfig = []
        materia['codigo'] = config.curso.codigo
        materia['nombre'] = config.curso.nombre
        materia['total'] = len(registrosConfig)
        totalGeneral += len(registrosConfig)

        programaMap = {}

        programas = Programa.objects.all()
        for pro in programas:
            if not programaMap.get(pro.codigo):
                programaMap[pro.codigo] = {}
                programaMap.get(pro.codigo)['nombre'] = pro.codigo
                programaMap.get(pro.codigo)['total'] = 0
                programaMap.get(pro.codigo)['asignados'] = 0
                programaMap.get(pro.codigo)['no_asignados'] = 0
            if not programaGeneralMap.get(pro.codigo):
                programaGeneralMap[pro.codigo] = {}
                programaGeneralMap.get(pro.codigo)['nombre'] = pro.codigo
                programaGeneralMap.get(pro.codigo)['total'] = 0
                programaGeneralMap.get(pro.codigo)['asignados'] = 0
                programaGeneralMap.get(pro.codigo)['no_asignados'] = 0

        nivelesMap = {'32-40': {'nombre': '32-40', 'total': 0, 'asignados': 0, 'no_asignados': 0},
                      '24-32': {'nombre': '24-32', 'total': 0, 'asignados': 0, 'no_asignados': 0},
                      '16-24': {'nombre': '16-24', 'total': 0, 'asignados': 0, 'no_asignados': 0},
                      '8-16': {'nombre': '8-16', 'total': 0, 'asignados': 0, 'no_asignados': 0},
                      '0-8': {'nombre': '0-8', 'total': 0, 'asignados': 0, 'no_asignados': 0}}

        cantidad = 0
        materia['estudiantes'] = {'asignados': [], 'no_asignados': []}
        # print config.curso.nombre
        for reg in registrosConfig:

            #TODO reubicar sobrantes
            programaEstudiante = ProgramaEstudiante.objects.filter(estudiante=reg.estudiante.pk, actual=True).get()
            programaMap.get(programaEstudiante.programa.codigo)['total'] = (programaMap.get(
                programaEstudiante.programa.codigo)['total']) + 1
            programaGeneralMap.get(programaEstudiante.programa.codigo)['total'] = (programaGeneralMap.get(
                programaEstudiante.programa.codigo)['total']) + 1
            try:
                porcentaje = PorcentajePrograma.objects.filter(configuracion_curso=config.pk,
                                                               programa=programaEstudiante.programa.pk).get()
                porcentajeAprobado = config.numero_estudiantes * porcentaje.porcentaje / 100
            except:
                porcentajeAprobado = 0

            #TODO ordenar por estudiante y creditos
            #TODO Aca estamos suponiendo que la suma de porcentajes NUNCA superara el 100%
            if config.numero_estudiantes > cantidad and porcentajeAprobado > 0:

                materia.get('estudiantes').get('asignados').append(
                    {'nombre': reg.estudiante.usuario.first_name, 'promedio': reg.estudiante.promedio(),
                     'edad': reg.estudiante.edad, 'programa': reg.estudiante.programa(),
                     'codigo': reg.estudiante.codigo})
                cantidad = cantidad + 1
                asignados = asignados + 1
                programaMap.get(programaEstudiante.programa.codigo)['asignados'] = \
                    programaMap.get(programaEstudiante.programa.codigo)['asignados'] + 1
                programaGeneralMap.get(programaEstudiante.programa.codigo)['asignados'] = \
                    programaGeneralMap.get(programaEstudiante.programa.codigo)['asignados'] + 1
                if reg.estudiante.promedio() > 32:
                    nivelesMap.get('32-40')['total'] = nivelesMap.get('32-40').get('total') + 1
                    nivelesMap.get('32-40')['asignados'] = nivelesMap.get('32-40').get('asignados') + 1
                    nivelesGeneralMap.get('32-40')['total'] = nivelesGeneralMap.get('32-40').get('total') + 1
                    nivelesGeneralMap.get('32-40')['asignados'] = nivelesGeneralMap.get('32-40').get(
                        'asignados') + 1
                elif reg.estudiante.promedio() > 24 and 32 >= reg.estudiante.promedio():
                    nivelesMap.get('24-32')['total'] = nivelesMap.get('24-32').get('total') + 1
                    nivelesMap.get('24-32')['asignados'] = nivelesMap.get('24-32').get('asignados') + 1
                    nivelesGeneralMap.get('24-32')['total'] = nivelesGeneralMap.get('24-32').get('total') + 1
                    nivelesGeneralMap.get('24-32')['asignados'] = nivelesGeneralMap.get('24-32').get(
                        'asignados') + 1
                elif reg.estudiante.promedio() > 16 and 24 >= reg.estudiante.promedio():
                    nivelesMap.get('16-24')['total'] = nivelesMap.get('16-24').get('total') + 1
                    nivelesMap.get('16-24')['asignados'] = nivelesMap.get('16-24').get('asignados') + 1
                    nivelesGeneralMap.get('16-24')['total'] = nivelesGeneralMap.get('16-24').get('total') + 1
                    nivelesGeneralMap.get('16-24')['asignados'] = nivelesGeneralMap.get('16-24').get(
                        'asignados') + 1
                elif reg.estudiante.promedio() > 8 and 16 >= reg.estudiante.promedio():
                    nivelesMap.get('8-16')['total'] = nivelesMap.get('8-16').get('total') + 1
                    nivelesMap.get('8-16')['asignados'] = nivelesMap.get('8-16').get('asignados') + 1
                    nivelesGeneralMap.get('8-16')['total'] = nivelesGeneralMap.get('8-16').get('total') + 1
                    nivelesGeneralMap.get('8-16')['asignados'] = nivelesGeneralMap.get('8-16').get('asignados') + 1
                elif 8 >= reg.estudiante.promedio():
                    nivelesMap.get('0-8')['total'] = nivelesMap.get('0-8').get('total') + 1
                    nivelesMap.get('0-8')['asignados'] = nivelesMap.get('0-8').get('asignados') + 1
                    nivelesGeneralMap.get('0-8')['total'] = nivelesGeneralMap.get('0-8').get('total') + 1
                    nivelesGeneralMap.get('0-8')['asignados'] = nivelesGeneralMap.get('0-8').get('asignados') + 1
            else:
                materia.get('estudiantes').get('no_asignados').append(
                    {'nombre': reg.estudiante.usuario.first_name, 'promedio': reg.estudiante.promedio(),
                     'edad': reg.estudiante.edad, 'programa': reg.estudiante.programa(),
                     'codigo': reg.estudiante.codigo})
                programaMap.get(programaEstudiante.programa.codigo)['no_asignados'] = \
                    programaMap.get(programaEstudiante.programa.codigo)['no_asignados'] + 1
                programaGeneralMap.get(programaEstudiante.programa.codigo)['no_asignados'] = \
                    programaGeneralMap.get(programaEstudiante.programa.codigo)['no_asignados'] + 1
                if reg.estudiante.promedio() > 32:
                    nivelesMap.get('32-40')['total'] = nivelesMap.get('32-40').get('total') + 1
                    nivelesMap.get('32-40')['no_asignados'] = nivelesMap.get('32-40').get('no_asignados') + 1
                    nivelesGeneralMap.get('32-40')['total'] = nivelesGeneralMap.get('32-40').get('total') + 1
                    nivelesGeneralMap.get('32-40')['no_asignados'] = nivelesGeneralMap.get('32-40').get(
                        'no_asignados') + 1
                elif reg.estudiante.promedio() > 24 and 32 >= reg.estudiante.promedio():
                    nivelesMap.get('24-32')['total'] = nivelesMap.get('24-32').get('total') + 1
                    nivelesMap.get('24-32')['no_asignados'] = nivelesMap.get('24-32').get('no_asignados') + 1
                    nivelesGeneralMap.get('24-32')['total'] = nivelesGeneralMap.get('24-32').get('total') + 1
                    nivelesGeneralMap.get('24-32')['no_asignados'] = nivelesGeneralMap.get('24-32').get(
                        'no_asignados') + 1
                elif reg.estudiante.promedio() > 16 and 24 >= reg.estudiante.promedio():
                    nivelesMap.get('16-24')['total'] = nivelesMap.get('16-24').get('total') + 1
                    nivelesMap.get('16-24')['no_asignados'] = nivelesMap.get('16-24').get('no_asignados') + 1
                    nivelesGeneralMap.get('16-24')['total'] = nivelesGeneralMap.get('16-24').get('total') + 1
                    nivelesGeneralMap.get('16-24')['no_asignados'] = nivelesGeneralMap.get('16-24').get(
                        'no_asignados') + 1
                elif reg.estudiante.promedio() > 8 and 16 >= reg.estudiante.promedio():
                    nivelesMap.get('8-16')['total'] = nivelesMap.get('8-16').get('total') + 1
                    nivelesMap.get('8-16')['no_asignados'] = nivelesMap.get('8-16').get('no_asignados') + 1
                    nivelesGeneralMap.get('8-16')['total'] = nivelesGeneralMap.get('8-16').get('total') + 1
                    nivelesGeneralMap.get('8-16')['no_asignados'] = nivelesGeneralMap.get('8-16').get(
                        'no_asignados') + 1
                elif 8 >= reg.estudiante.promedio():
                    nivelesMap.get('0-8')['total'] = nivelesMap.get('0-8').get('total') + 1
                    nivelesMap.get('0-8')['no_asignados'] = nivelesMap.get('0-8').get('no_asignados') + 1
                    nivelesGeneralMap.get('0-8')['total'] = nivelesGeneralMap.get('0-8').get('total') + 1
                    nivelesGeneralMap.get('0-8')['no_asignados'] = nivelesGeneralMap.get('0-8').get(
                        'no_asignados') + 1
        materia['asignados'] = cantidad
        materia['no_asignados'] = len(registrosConfig) - cantidad
        if len(registrosConfig) == 0:
            materia['efectividad'] = 100;
        else:
            materia['efectividad'] = cantidad * 100 / len(registrosConfig)
        materia['programas'] = programaMap.values()
        materia['niveles'] = nivelesMap.values()
        materias.append(materia)
    grupos['programas'] = programaGeneralMap.values()
    grupos['niveles'] = nivelesGeneralMap.values()
    data['total'] = totalGeneral
    data['asignados'] = asignados
    data['no_asignados'] = totalGeneral - asignados
    data['periodo'] = periodo.nombre
    if totalGeneral == 0:
        data['efectividad'] = 0
    else:
        data['efectividad'] = asignados * 100 / totalGeneral
    data['materias'] = sorted(materias, key=lambda m: m['efectividad'], reverse=True)
    data['grupos'] = grupos

    return HttpResponse(json.dumps(data), content_type='application/json; charset=UTF-8')


@user_passes_test(coordinador_check, login_url='/login/')
def optimizadorExample(request):
    data = {'efectividad': 15,
            'periodo': '2015-10',
            'total': 1200,
            'asignados': 940,
            'no_asignados': 260,
            'materias': [{
                             'codigo': 'CODIGO',
                             'nombre': 'Curso 1',
                             'efectividad': 80,
                             'total': 120,
                             'asignados': 80,
                             'no_asignados': 40,
                             'programas': [{'nombre': 'MISO', 'total': 15, 'asignados': 5, 'no_asignados': 10},
                                           {'nombre': 'MATI', 'total': 55, 'asignados': 50, 'no_asignados': 5},
                                           {'nombre': 'MESI', 'total': 10, 'asignados': 10, 'no_asignados': 0},
                                           {'nombre': 'MBC', 'total': 0, 'asignados': 0, 'no_asignados': 0},
                                           {'nombre': 'MISIS', 'total': 5, 'asignados': 5, 'no_asignados': 0},
                                           {'nombre': 'MBIT', 'total': 30, 'asignados': 10, 'no_asignados': 20},
                                           {'nombre': 'Pregrado', 'total': 3, 'asignados': 0, 'no_asignados': 3},
                                           {'nombre': 'Doctorado', 'total': 2, 'asignados': 0, 'no_asignados': 2}],
                             'niveles': [{'nombre': '32-40', 'total': 5, 'asignados': 5, 'no_asignados': 0},
                                         {'nombre': '24-32', 'total': 15, 'asignados': 5, 'no_asignados': 10},
                                         {'nombre': '16-24', 'total': 10, 'asignados': 10, 'no_asignados': 0},
                                         {'nombre': '8-16', 'total': 60, 'asignados': 40, 'no_asignados': 20},
                                         {'nombre': '0-8', 'total': 30, 'asignados': 20, 'no_asignados': 10}],
                             'estudiantes': [{}],
                         }, {
                             'codigo': 'CODIGO',
                             'nombre': 'Curso 2',
                             'efectividad': 85,
                             'total': 70,
                             'asignados': 30,
                             'no_asignados': 40,
                             'programas': [{'nombre': 'MISO', 'total': 15, 'asignados': 5, 'no_asignados': 10},
                                           {'nombre': 'MATI', 'total': 55, 'asignados': 50, 'no_asignados': 5},
                                           {'nombre': 'MESI', 'total': 10, 'asignados': 10, 'no_asignados': 0},
                                           {'nombre': 'MBC', 'total': 0, 'asignados': 0, 'no_asignados': 0},
                                           {'nombre': 'MISIS', 'total': 5, 'asignados': 5, 'no_asignados': 0},
                                           {'nombre': 'MBIT', 'total': 30, 'asignados': 10, 'no_asignados': 20},
                                           {'nombre': 'Pregrado', 'total': 3, 'asignados': 0, 'no_asignados': 3},
                                           {'nombre': 'Doctorado', 'total': 2, 'asignados': 0, 'no_asignados': 2}],
                             'niveles': [{'nombre': '32-40', 'total': 5, 'asignados': 5, 'no_asignados': 0},
                                         {'nombre': '24-32', 'total': 15, 'asignados': 5, 'no_asignados': 10},
                                         {'nombre': '16-24', 'total': 10, 'asignados': 10, 'no_asignados': 0},
                                         {'nombre': '8-16', 'total': 60, 'asignados': 40, 'no_asignados': 20},
                                         {'nombre': '0-8', 'total': 30, 'asignados': 20, 'no_asignados': 10}],
                             'estudiantes': [{}],
                         }, {
                             'codigo': 'CODIGO',
                             'nombre': 'Curso 3',
                             'efectividad': 45,
                             'total': 30,
                             'asignados': 30,
                             'no_asignados': 0,
                             'programas': [{'nombre': 'MISO', 'total': 15, 'asignados': 5, 'no_asignados': 10},
                                           {'nombre': 'MATI', 'total': 55, 'asignados': 50, 'no_asignados': 5},
                                           {'nombre': 'MESI', 'total': 10, 'asignados': 10, 'no_asignados': 0},
                                           {'nombre': 'MBC', 'total': 0, 'asignados': 0, 'no_asignados': 0},
                                           {'nombre': 'MISIS', 'total': 5, 'asignados': 5, 'no_asignados': 0},
                                           {'nombre': 'MBIT', 'total': 30, 'asignados': 10, 'no_asignados': 20},
                                           {'nombre': 'Pregrado', 'total': 3, 'asignados': 0, 'no_asignados': 3},
                                           {'nombre': 'Doctorado', 'total': 2, 'asignados': 0, 'no_asignados': 2}],
                             'niveles': [{'nombre': '32-40', 'total': 5, 'asignados': 5, 'no_asignados': 0},
                                         {'nombre': '24-32', 'total': 15, 'asignados': 5, 'no_asignados': 10},
                                         {'nombre': '16-24', 'total': 10, 'asignados': 10, 'no_asignados': 0},
                                         {'nombre': '8-16', 'total': 60, 'asignados': 40, 'no_asignados': 20},
                                         {'nombre': '0-8', 'total': 30, 'asignados': 20, 'no_asignados': 10}],
                             'estudiantes': [{}],
                         }, {
                             'codigo': 'CODIGO',
                             'nombre': 'Curso 4',
                             'efectividad': 40,
                             'total': 40,
                             'asignados': 40,
                             'no_asignados': 0,
                             'programas': [{'nombre': 'MISO', 'total': 15, 'asignados': 5, 'no_asignados': 10},
                                           {'nombre': 'MATI', 'total': 55, 'asignados': 50, 'no_asignados': 5},
                                           {'nombre': 'MESI', 'total': 10, 'asignados': 10, 'no_asignados': 0},
                                           {'nombre': 'MBC', 'total': 0, 'asignados': 0, 'no_asignados': 0},
                                           {'nombre': 'MISIS', 'total': 5, 'asignados': 5, 'no_asignados': 0},
                                           {'nombre': 'MBIT', 'total': 30, 'asignados': 10, 'no_asignados': 20},
                                           {'nombre': 'Pregrado', 'total': 3, 'asignados': 0, 'no_asignados': 3},
                                           {'nombre': 'Doctorado', 'total': 2, 'asignados': 0, 'no_asignados': 2}],
                             'niveles': [{'nombre': '32-40', 'total': 5, 'asignados': 5, 'no_asignados': 0},
                                         {'nombre': '24-32', 'total': 15, 'asignados': 5, 'no_asignados': 10},
                                         {'nombre': '16-24', 'total': 10, 'asignados': 10, 'no_asignados': 0},
                                         {'nombre': '8-16', 'total': 60, 'asignados': 40, 'no_asignados': 20},
                                         {'nombre': '0-8', 'total': 30, 'asignados': 20, 'no_asignados': 10}],
                             'estudiantes': [{}],
                         }, {
                             'codigo': 'CODIGO',
                             'nombre': 'Curso 5',
                             'efectividad': 40,
                             'total': 55,
                             'asignados': 40,
                             'no_asignados': 15,
                             'programas': [{'nombre': 'MISO', 'total': 15, 'asignados': 5, 'no_asignados': 10},
                                           {'nombre': 'MATI', 'total': 55, 'asignados': 50, 'no_asignados': 5},
                                           {'nombre': 'MESI', 'total': 10, 'asignados': 10, 'no_asignados': 0},
                                           {'nombre': 'MBC', 'total': 0, 'asignados': 0, 'no_asignados': 0},
                                           {'nombre': 'MISIS', 'total': 5, 'asignados': 5, 'no_asignados': 0},
                                           {'nombre': 'MBIT', 'total': 30, 'asignados': 10, 'no_asignados': 20},
                                           {'nombre': 'Pregrado', 'total': 3, 'asignados': 0, 'no_asignados': 3},
                                           {'nombre': 'Doctorado', 'total': 2, 'asignados': 0, 'no_asignados': 2}],
                             'niveles': [{'nombre': '32-40', 'total': 5, 'asignados': 5, 'no_asignados': 0},
                                         {'nombre': '24-32', 'total': 15, 'asignados': 5, 'no_asignados': 10},
                                         {'nombre': '16-24', 'total': 10, 'asignados': 10, 'no_asignados': 0},
                                         {'nombre': '8-16', 'total': 60, 'asignados': 40, 'no_asignados': 20},
                                         {'nombre': '0-8', 'total': 30, 'asignados': 20, 'no_asignados': 10}],
                             'estudiantes': [{}],
                         }, {
                             'codigo': 'CODIGO',
                             'nombre': 'Curso 6',
                             'efectividad': 40,
                             'total': 130,
                             'asignados': 40,
                             'no_asignados': 90,
                             'programas': [{'nombre': 'MISO', 'total': 15, 'asignados': 5, 'no_asignados': 10},
                                           {'nombre': 'MATI', 'total': 55, 'asignados': 50, 'no_asignados': 5},
                                           {'nombre': 'MESI', 'total': 10, 'asignados': 10, 'no_asignados': 0},
                                           {'nombre': 'MBC', 'total': 0, 'asignados': 0, 'no_asignados': 0},
                                           {'nombre': 'MISIS', 'total': 5, 'asignados': 5, 'no_asignados': 0},
                                           {'nombre': 'MBIT', 'total': 30, 'asignados': 10, 'no_asignados': 20},
                                           {'nombre': 'Pregrado', 'total': 3, 'asignados': 0, 'no_asignados': 3},
                                           {'nombre': 'Doctorado', 'total': 2, 'asignados': 0, 'no_asignados': 2}],
                             'niveles': [{'nombre': '32-40', 'total': 5, 'asignados': 5, 'no_asignados': 0},
                                         {'nombre': '24-32', 'total': 15, 'asignados': 5, 'no_asignados': 10},
                                         {'nombre': '16-24', 'total': 10, 'asignados': 10, 'no_asignados': 0},
                                         {'nombre': '8-16', 'total': 60, 'asignados': 40, 'no_asignados': 20},
                                         {'nombre': '0-8', 'total': 30, 'asignados': 20, 'no_asignados': 10}],
                             'estudiantes': [{}],
                         }, {
                             'codigo': 'CODIGO',
                             'nombre': 'Curso 7',
                             'efectividad': 40,
                             'total': 120,
                             'asignados': 80,
                             'no_asignados': 40,
                             'programas': [{'nombre': 'MISO', 'total': 15, 'asignados': 5, 'no_asignados': 10},
                                           {'nombre': 'MATI', 'total': 55, 'asignados': 50, 'no_asignados': 5},
                                           {'nombre': 'MESI', 'total': 10, 'asignados': 10, 'no_asignados': 0},
                                           {'nombre': 'MBC', 'total': 0, 'asignados': 0, 'no_asignados': 0},
                                           {'nombre': 'MISIS', 'total': 5, 'asignados': 5, 'no_asignados': 0},
                                           {'nombre': 'MBIT', 'total': 30, 'asignados': 10, 'no_asignados': 20},
                                           {'nombre': 'Pregrado', 'total': 3, 'asignados': 0, 'no_asignados': 3},
                                           {'nombre': 'Doctorado', 'total': 2, 'asignados': 0, 'no_asignados': 2}],
                             'niveles': [{'nombre': '32-40', 'total': 5, 'asignados': 5, 'no_asignados': 0},
                                         {'nombre': '24-32', 'total': 15, 'asignados': 5, 'no_asignados': 10},
                                         {'nombre': '16-24', 'total': 10, 'asignados': 10, 'no_asignados': 0},
                                         {'nombre': '8-16', 'total': 60, 'asignados': 40, 'no_asignados': 20},
                                         {'nombre': '0-8', 'total': 30, 'asignados': 20, 'no_asignados': 10}],
                             'estudiantes': [{}],
                         }, {
                             'codigo': 'CODIGO',
                             'nombre': 'Curso 8',
                             'efectividad': 40,
                             'total': 120,
                             'asignados': 40,
                             'no_asignados': 80,
                             'programas': [{'nombre': 'MISO', 'total': 15, 'asignados': 5, 'no_asignados': 10},
                                           {'nombre': 'MATI', 'total': 55, 'asignados': 50, 'no_asignados': 5},
                                           {'nombre': 'MESI', 'total': 10, 'asignados': 10, 'no_asignados': 0},
                                           {'nombre': 'MBC', 'total': 0, 'asignados': 0, 'no_asignados': 0},
                                           {'nombre': 'MISIS', 'total': 5, 'asignados': 5, 'no_asignados': 0},
                                           {'nombre': 'MBIT', 'total': 30, 'asignados': 10, 'no_asignados': 20},
                                           {'nombre': 'Pregrado', 'total': 3, 'asignados': 0, 'no_asignados': 3},
                                           {'nombre': 'Doctorado', 'total': 2, 'asignados': 0, 'no_asignados': 2}],
                             'niveles': [{'nombre': '32-40', 'total': 5, 'asignados': 5, 'no_asignados': 0},
                                         {'nombre': '24-32', 'total': 15, 'asignados': 5, 'no_asignados': 10},
                                         {'nombre': '16-24', 'total': 10, 'asignados': 10, 'no_asignados': 0},
                                         {'nombre': '8-16', 'total': 60, 'asignados': 40, 'no_asignados': 20},
                                         {'nombre': '0-8', 'total': 30, 'asignados': 20, 'no_asignados': 10}],
                             'estudiantes': [{}],
                         }, {
                             'codigo': 'CODIGO',
                             'nombre': 'Curso 9',
                             'efectividad': 40,
                             'total': 120,
                             'asignados': 40,
                             'no_asignados': 80,
                             'programas': [{'nombre': 'MISO', 'total': 15, 'asignados': 5, 'no_asignados': 10},
                                           {'nombre': 'MATI', 'total': 55, 'asignados': 50, 'no_asignados': 5},
                                           {'nombre': 'MESI', 'total': 10, 'asignados': 10, 'no_asignados': 0},
                                           {'nombre': 'MBC', 'total': 0, 'asignados': 0, 'no_asignados': 0},
                                           {'nombre': 'MISIS', 'total': 5, 'asignados': 5, 'no_asignados': 0},
                                           {'nombre': 'MBIT', 'total': 30, 'asignados': 10, 'no_asignados': 20},
                                           {'nombre': 'Pregrado', 'total': 3, 'asignados': 0, 'no_asignados': 3},
                                           {'nombre': 'Doctorado', 'total': 2, 'asignados': 0, 'no_asignados': 2}],
                             'niveles': [{'nombre': '32-40', 'total': 5, 'asignados': 5, 'no_asignados': 0},
                                         {'nombre': '24-32', 'total': 15, 'asignados': 5, 'no_asignados': 10},
                                         {'nombre': '16-24', 'total': 10, 'asignados': 10, 'no_asignados': 0},
                                         {'nombre': '8-16', 'total': 60, 'asignados': 40, 'no_asignados': 20},
                                         {'nombre': '0-8', 'total': 30, 'asignados': 20, 'no_asignados': 10}],
                             'estudiantes': [{}],
                         }, {
                             'codigo': 'CODIGO',
                             'nombre': 'Curso 10',
                             'efectividad': 40,
                             'total': 100,
                             'asignados': 40,
                             'no_asignados': 60,
                             'programas': [{'nombre': 'MISO', 'total': 15, 'asignados': 5, 'no_asignados': 10},
                                           {'nombre': 'MATI', 'total': 55, 'asignados': 50, 'no_asignados': 5},
                                           {'nombre': 'MESI', 'total': 10, 'asignados': 10, 'no_asignados': 0},
                                           {'nombre': 'MBC', 'total': 0, 'asignados': 0, 'no_asignados': 0},
                                           {'nombre': 'MISIS', 'total': 5, 'asignados': 5, 'no_asignados': 0},
                                           {'nombre': 'MBIT', 'total': 30, 'asignados': 10, 'no_asignados': 20},
                                           {'nombre': 'Pregrado', 'total': 3, 'asignados': 0, 'no_asignados': 3},
                                           {'nombre': 'Doctorado', 'total': 2, 'asignados': 0, 'no_asignados': 2}],
                             'niveles': [{'nombre': '32-40', 'total': 5, 'asignados': 5, 'no_asignados': 0},
                                         {'nombre': '24-32', 'total': 15, 'asignados': 5, 'no_asignados': 10},
                                         {'nombre': '16-24', 'total': 10, 'asignados': 10, 'no_asignados': 0},
                                         {'nombre': '8-16', 'total': 60, 'asignados': 40, 'no_asignados': 20},
                                         {'nombre': '0-8', 'total': 30, 'asignados': 20, 'no_asignados': 10}],
                             'estudiantes': [{}],
                         }],
            'grupos': {'programas': [{'nombre': 'MISO', 'total': 300, 'asignados': 150, 'no_asignados': 150},
                                     {'nombre': 'MATI', 'total': 320, 'asignados': 280, 'no_asignados': 40},
                                     {'nombre': 'MESI', 'total': 200, 'asignados': 150, 'no_asignados': 50},
                                     {'nombre': 'MBC', 'total': 80, 'asignados': 70, 'no_asignados': 10},
                                     {'nombre': 'MISIS', 'total': 50, 'asignados': 50, 'no_asignados': 0},
                                     {'nombre': 'MBIT', 'total': 200, 'asignados': 130, 'no_asignados': 70},
                                     {'nombre': 'Pregrado', 'total': 30, 'asignados': 15, 'no_asignados': 15},
                                     {'nombre': 'Doctorado', 'total': 20, 'asignados': 18, 'no_asignados': 2}],
                       'niveles': [{'nombre': '32-40', 'total': 100, 'asignados': 30, 'no_asignados': 70},
                                   {'nombre': '24-32', 'total': 200, 'asignados': 150, 'no_asignados': 50},
                                   {'nombre': '16-24', 'total': 200, 'asignados': 200, 'no_asignados': 0},
                                   {'nombre': '8-16', 'total': 400, 'asignados': 300, 'no_asignados': 100},
                                   {'nombre': '0-8', 'total': 300, 'asignados': 300, 'no_asignados': 0}]}
    }
    return HttpResponse(json.dumps(data), content_type='application/json; charset=UTF-8')


@user_passes_test(coordinador_check, login_url='/login/')
def crearHistorico(request):
    estudiantes = Estudiante.objects.all()
    for est in estudiantes:
        programa = Programa.objects.select_related('ProgramaEstudiante').filter(
            programaestudiante__estudiante__id=est.id)
        cursos_programa = Curso.objects.filter(programa=programa)
        cursos_no_programa = Curso.objects.exclude(programa=programa)
        num = random.random()
        num_cursos = int((num * 10))
        for i in range(num_cursos):
            num2 = random.random()
            if num2 > 0.75:
                num3 = random.random()
                num3 = int(num3)
                curso_a_aniadir = len(cursos_programa)
                curso_a_aniadir = curso_a_aniadir * num3
                curso_a_aniadir = int(curso_a_aniadir)
                curso = cursos_programa[curso_a_aniadir]
                tipo_registro = TipoRegistroCurso.objects.get(nombre='APROBADO')
                semestre = Semestre.objects.get(nombre='2014-10')
                registro = RegistroCurso.objects.create(semestre=semestre, curso=curso, estudiante=est,
                                                        tipo_registro=tipo_registro)
                registro.save()
            else:
                num3 = random.random()
                curso_a_aniadir = len(cursos_no_programa)
                curso_a_aniadir = curso_a_aniadir * num3
                curso_a_aniadir = int(curso_a_aniadir)
                curso = cursos_no_programa[curso_a_aniadir]
                tipo_registro = TipoRegistroCurso.objects.get(nombre='APROBADO')
                semestre = Semestre.objects.get(nombre='2014-10')
                registro = RegistroCurso.objects.create(semestre=semestre, curso=curso, estudiante=est,
                                                        tipo_registro=tipo_registro)
                registro.save()

    return HttpResponse(json.dumps('Got it'), content_type='application/json; charset=UTF-8')


@user_passes_test(coordinador_check, login_url='/login/')
def crearDemanda(request):
    # Traemos todos los estudiantes activos
    estudiantes = Estudiante.objects.filter(estado=True).all()

    coordinador = request.user.coordinador
    # Conseguir programa del coordinador
    programa = Programa.objects.filter(coordinador=coordinador.pk).get()

    # Traemos todos los cursos del programa
    cursos = Curso.objects.filter(programa=programa.pk).all()

    #Calculamos la cantidad del grupo de estudiantes
    step = len(estudiantes) / len(cursos)
    cantidad = len(estudiantes)

    #Traemos los tipos de registros planeado, en curso y aprobado para descartar a los estudiantes que no cumplen
    tipoRegistros = TipoRegistroCurso.objects.filter(nombre__in=['PLANEADO', 'EN CURSO', 'APROBADO']).all().values('id')

    #Traemos el registro con el que guardaremos
    tipoRegis = TipoRegistroCurso.objects.filter(nombre='PLANEADO').get()

    #Traemos el semestre para el que aplica nuestra demanda
    semestre = Semestre.objects.filter(fecha_inicial__gte=datetime.datetime.now()).order_by('fecha_inicial')[0]

    #Recorremos los cursos
    for curso in cursos:

        #Traemos la cantidad de estudiantes que ya tienen registrado el curso para tenerlo en cuenta en la asignación
        registros = RegistroCurso.objects.filter(curso=curso.pk, tipo_registro__in=tipoRegistros,
                                                 semestre=semestre.pk).all()
        pos = len(registros)

        #Recorremos los estudiantes
        for est in estudiantes:
            registros = RegistroCurso.objects.filter(estudiante=est.pk, tipo_registro=tipoRegis,
                                                        semestre=semestre.pk).all()
            if len(registros)<=3:
                #Calculamos cuantos estudiantes mas necesitamos en esta asignación
                if pos < cantidad:

                    #Verificamos que el estudiante que estamos utilizando no la tenga inscrita
                    registro = RegistroCurso.objects.filter(estudiante=est.pk, tipo_registro__in=tipoRegistros,
                                                            semestre=semestre.pk, curso=curso.pk)
                    #Si no la tiene inscrita
                    if not registro:
                        RegistroCurso.objects.create(estudiante=est, semestre=semestre, tipo_registro=tipoRegis,
                                                     curso=curso).save()
                        pos += 1

        #Le restamos a la cantidad que necesitamos escribir el step o cantidad del grupo de estudiantes
        cantidad -= step

    return HttpResponse(json.dumps('Got it'), content_type='application/json; charset=UTF-8')


@user_passes_test(coordinador_check, login_url='/login/')
def getConfiguracion(request):
    coordinador = request.user.coordinador
    # Conseguir programa del coordinador
    programa = Programa.objects.filter(coordinador=coordinador.pk).get()
    programas = Programa.objects.filter(estado=True).values('codigo', 'id').all()
    cursos = Curso.objects.filter(programa=programa.pk).all()
    semestre = Semestre.objects.filter(fecha_inicial__gte=datetime.datetime.now()).order_by('fecha_inicial')[0]
    configuraciones = ConfiguracionCurso.objects.filter(semestre=semestre, curso__in=cursos)
    dataProgramas = []
    for programa in programas:
        record = {
            'nombre': programa['codigo'],
            'id': programa['id']
        }
        dataProgramas.append(record)

    dataCursos = []
    for curso in cursos:
        dataPorcentajes = []
        try:
            numero = configuraciones.get(curso=curso).numero_estudiantes
            porcentajes = PorcentajePrograma.objects.filter(configuracion_curso=configuraciones.get(curso=curso))
            for por in porcentajes:
                if por.porcentaje > 0:
                    record = {
                        'nombre': por.programa.codigo,
                        'id': por.programa_id,
                        'porcentaje': por.porcentaje,
                    }
                    dataPorcentajes.append(record)
        except ConfiguracionCurso.DoesNotExist:
            numero = 0
        record = {
            'nombre': curso.nombre,
            'codigo': curso.codigo,
            'descripcion': curso.descripcion,
            'id': curso.id,
            'porcentajes': dataPorcentajes,
            'estudiantes': numero,
        }
        dataCursos.append(record)
    data = {'programas': dataProgramas, 'cursos': dataCursos, 'periodo': semestre.nombre}
    return HttpResponse(json.dumps(data), content_type='application/json; charset=UTF-8')


@user_passes_test(coordinador_check, login_url='/login/')
def guardarConfiguracion(request):
    result = {'success': False, 'message': ''}
    map = request.POST.get('map', False)
    map = json.loads(map)

    coordinador = request.user.coordinador
    try:
        # Conseguir programa del coordinador
        programa = Programa.objects.filter(coordinador=coordinador.pk).get()
        # Conseguir periodo siguiente
        periodo = Semestre.objects.filter(planeador_estudio=1, estado=True,
                                          fecha_inicial__gte=datetime.datetime.now()).order_by('fecha_inicial')[0]
        # Traer los registros del periodo siguiente de cursos que pertenecen al programa
        cursos = Curso.objects.filter(programa=programa.pk).all()

        for curso in cursos:
            data = map[str(curso.pk)]

            try:
                config = ConfiguracionCurso.objects.filter(curso=curso, semestre=periodo).get()
                PorcentajePrograma.objects.filter(configuracion_curso=config).delete()
                config.numero_estudiantes = long(data['estudiantes'])
                config.save()

            except ConfiguracionCurso.DoesNotExist:
                config = ConfiguracionCurso(semestre=periodo, numero_estudiantes=long(data['estudiantes']),
                                            curso=curso)
                config.save()

            for pro in data['porcentajes']:
                prog = Programa.objects.get(pk=long(pro['id']))
                p = PorcentajePrograma(porcentaje=int(pro['porcentaje']), programa=prog, configuracion_curso=config)
                p.save()

        result['success'] = True
        result['message'] = 'La configuración para el siguiente periodo ha sido guardada'
    except Exception:
        result['message'] = 'Error en la creación de la nueva configuración'

    # for key in map:
    # if int(key) != 0:
    # print key
    # print map[key]
    #        config = ConfiguracionCurso.objects.filter(curso__id=int(key),semestre=semestre)

    return HttpResponse(json.dumps(result), content_type='application/json; charset=UTF-8')


def schoodle_login(request):
    # Está solicitando autenticación
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                user_type = Estudiante.autorizacion(user.id)
                if user_type == "estudiante":
                    return redirect('planeador')
                elif user_type == "coordinador":
                    return redirect('optimizador')
                # El usuario es un superusuario
                else:
                    return redirect('/admin/')
            else:
                messages = {'mensaje': "El usuario no está activo."}
                return render_to_response('login.html', messages, context_instance=RequestContext(request))
        else:
            messages = {'mensaje': "El usuario o contraseña que ha introducido no son correctas."}
            return render_to_response('login.html', messages, context_instance=RequestContext(request))
    # Es una peticion normal y ya existe una sesión previa
    else:
        if "_auth_user_id" in request.session:
            user_id = request.session["_auth_user_id"]
            user_type = Estudiante.autorizacion(user_id)
            if user_type == "estudiante":
                return redirect('home')
            elif user_type == "coordinador":
                return redirect('optimizador')
            else:
                return redirect('/admin/')
        else:
            return render_to_response('login.html', {}, context_instance=RequestContext(request))


def schoodle_logout(request):
    logout(request)
    return redirect('login')


@user_passes_test(coordinador_check, login_url='/login/')
def getListaEstudiantes(request):
    coordinador = request.user.coordinador
    programa = Programa.objects.filter(coordinador=coordinador.pk).get()
    estudiantes = programa.listaEstudiantePorPrograma()
    result = []
    segment = {}
    letter = None
    for estudiante in estudiantes:
        letter = estudiante.usuario.last_name[0].upper()
        if 'letter' in segment:
            if letter != segment['letter']:
                result.append(segment)
                segment = {'letter': letter, 'students': []}
        else:
            segment = {'letter': letter, 'students': []}
        record = {
            'id': estudiante.id,
            'nombre': estudiante.usuario.first_name,
            'apellido': estudiante.usuario.last_name,
        }
        segment['students'].append(record)
    result.append(segment)
    return HttpResponse(json.dumps(result), content_type='application/json; charset=UTF-8')


@user_passes_test(coordinador_check, login_url='/login/')
def obtenerDatosEstudiante(request, idestudiante):
    coordinador = request.user.coordinador
    data = {}
    if coordinador:
        try:
            user = Estudiante.objects.get(pk=idestudiante)
            userId = idestudiante
        except Estudiante.DoesNotExist:
            user = None

        if user:
            nombreEstudiante = user.usuario.first_name + " " + user.usuario.last_name
            correo = user.usuario.email

            # PROGRAMA DEL ESTUDIANTE
            prog_est = ProgramaEstudiante.objects.filter(actual=1).get(estudiante=userId)
            programaEstudiante = prog_est.programa.nombre
            list_registro_curso_user = RegistroCurso.objects.filter(tipo_registro=1, estudiante=userId);
            # CREDITOS APROBADOS
            creditos_aprobados_curso_user = 0
            # for cursos in list_registro_curso_user:
            # creditos_aprobados_curso_user += cursos.curso.creditos
            # CREDITOS PLANEADOS
            creditos_totales_planeador = 0
            #CREDITOS EN CURSO
            creditos_totales_en_curso = 0
            # CREDITOS FALTANTES
            creditos_faltantes_Programa = prog_est.programa.creditos_totales
            creditos_faltantes = creditos_faltantes_Programa - creditos_aprobados_curso_user
            # CREDITOS VISTOS
            registrosAprobados = RegistroCurso.objects.filter(estudiante=userId, tipo_registro__nombre='APROBADO').all()
            for registrosAP in registrosAprobados:
                creditos_aprobados_curso_user += registrosAP.curso.creditos
                # CREDITOS PLANEADOS
            registrosPlaneados = RegistroCurso.objects.filter(estudiante=userId, tipo_registro__nombre='PLANEADO').all()
            for registrosPlane in registrosPlaneados:
                creditos_totales_planeador += registrosPlane.curso.creditos
            # CREDITOS ACTUALES
            registrosActuales = RegistroCurso.objects.filter(estudiante=userId, tipo_registro__nombre='EN CURSO').all()
            for registrosActuales in registrosActuales:
                creditos_totales_en_curso += registrosActuales.curso.creditos

            cursosAprobados = Curso.darMateriasVistas(userId);
            cursosEnCurso = Curso.darMateriasCursando(userId);
            cursosPlaneados = Curso.darMateriasPlaneadas(userId);
            # tomamos creditos totales del planeador del estudiante
            import time

            current_year = time.strftime("%Y")
            current_month = time.strftime("%m")
            if current_month <= 4:
                future_semesters = [current_year + "-18", current_year + "-20",
                                    str(int(current_year) + 1) + "-10", str(int(current_year) + 1) + "-18"]
            elif current_month in [6, 5]:
                future_semesters = [current_year + "-20", str(int(current_year) + 1) + "-01",
                                    str(int(current_year) + 1) + "-18", str(int(current_year) + 1) + "-20"]
            else:
                future_semesters = [str(int(current_year) + 1) + "-10", str(int(current_year) + 1) + "-18",
                                    str(int(current_year) + 1) + "-20", str(int(current_year) + 2) + "-10"]

            list_semesters = Semestre.objects.filter(planeador_estudio=1, estado=True,
                                                     nombre__in=future_semesters).all()
            list_registers = RegistroCurso.objects.filter(estudiante=user.pk, semestre__in=list_semesters,
                                                          tipo_registro="3").all()
            for semester in list_semesters:
                list_temp = []
                for register in list_registers:
                    # print register
                    if register.semestre == semester:
                        list_temp.append({'curso': register.curso, 'id': register.id})
                        creditos_totales_planeador += register.curso.creditos
            data = {'nombreEstudiante': nombreEstudiante, 'programa': programaEstudiante, 'correo': correo,
                    'creditos_aprobados_curso_user': creditos_aprobados_curso_user,
                    'creditos_totales_en_curso': creditos_totales_en_curso,
                    'creditos_totales_planeador': creditos_totales_planeador,
                    'creditos_totales_en_curso': creditos_totales_en_curso, 'creditos_faltantes': creditos_faltantes,
                    'cursos_aprobados': [],
                    'cursos_planeados': [], 'cursos_en_curso': [], }
            for curso in cursosAprobados:
                data['cursos_aprobados'].append(curso.json())
            for curso in cursosPlaneados:
                data['cursos_planeados'].append(curso.json())
            for curso in cursosEnCurso:
                data['cursos_en_curso'].append(curso.json())
        else:
            data = {'response': 'failed'}
    return HttpResponse(json.dumps(data), content_type='application/json; charset=UTF-8')

