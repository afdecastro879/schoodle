# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Semestre.fecha_inicial'
        db.add_column(u'coordinador_semestre', 'fecha_inicial',
                      self.gf('django.db.models.fields.DateField')(default=datetime.datetime(2014, 9, 21, 0, 0)),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Semestre.fecha_inicial'
        db.delete_column(u'coordinador_semestre', 'fecha_inicial')


    models = {
        u'coordinador.curso': {
            'Meta': {'ordering': "('nombre',)", 'object_name': 'Curso'},
            'codigo': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'creditos': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'creditos_minimos': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'es_electiva': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'estado': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'estudiantes': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['coordinador.Estudiante']", 'through': u"orm['coordinador.RegistroCurso']", 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'programa': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.Programa']"})
        },
        u'coordinador.estudiante': {
            'Meta': {'ordering': "('nombre',)", 'object_name': 'Estudiante'},
            'edad': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'estado': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'foto': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        u'coordinador.planeadorestudio': {
            'Meta': {'ordering': "('nombre',)", 'object_name': 'PlaneadorEstudio'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'num_sem_plan': ('django.db.models.fields.IntegerField', [], {'default': '4'})
        },
        u'coordinador.programa': {
            'Meta': {'ordering': "('nombre',)", 'object_name': 'Programa'},
            'codigo': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'creditos_totales': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'estado': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'estudiantes': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['coordinador.Estudiante']", 'through': u"orm['coordinador.ProgramaEstudiante']", 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'planeador_estudio': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.PlaneadorEstudio']"}),
            'tipo_programa': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.TipoPrograma']"})
        },
        u'coordinador.programaestudiante': {
            'Meta': {'ordering': "('programa',)", 'object_name': 'ProgramaEstudiante'},
            'actual': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'estudiante': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.Estudiante']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'programa': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.Programa']"})
        },
        u'coordinador.registrocurso': {
            'Meta': {'ordering': "('curso',)", 'object_name': 'RegistroCurso'},
            'curso': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.Curso']"}),
            'estudiante': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.Estudiante']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'semestre': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.Semestre']"}),
            'tipo_registro': ('django.db.models.fields.related.ForeignKey', [], {'default': '3', 'to': u"orm['coordinador.TipoRegistroCurso']"})
        },
        u'coordinador.semestre': {
            'Meta': {'ordering': "('nombre',)", 'object_name': 'Semestre'},
            'estado': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'fecha_inicial': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '7'}),
            'planeador_estudio': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.PlaneadorEstudio']"})
        },
        u'coordinador.tipoprograma': {
            'Meta': {'ordering': "('nombre',)", 'object_name': 'TipoPrograma'},
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'estado': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'coordinador.tiporegistrocurso': {
            'Meta': {'ordering': "('nombre',)", 'object_name': 'TipoRegistroCurso'},
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'estado': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['coordinador']