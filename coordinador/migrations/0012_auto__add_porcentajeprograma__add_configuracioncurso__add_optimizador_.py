# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'PorcentajePrograma'
        db.create_table(u'coordinador_porcentajeprograma', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('programa', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['coordinador.Programa'])),
            ('porcentaje', self.gf('django.db.models.fields.IntegerField')()),
            ('configuracion_curso', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['coordinador.ConfiguracionCurso'])),
        ))
        db.send_create_signal(u'coordinador', ['PorcentajePrograma'])

        # Adding model 'ConfiguracionCurso'
        db.create_table(u'coordinador_configuracioncurso', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('semestre', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['coordinador.Semestre'])),
            ('numero_estudiantes', self.gf('django.db.models.fields.IntegerField')()),
            ('curso', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['coordinador.Curso'])),
        ))
        db.send_create_signal(u'coordinador', ['ConfiguracionCurso'])

        # Adding model 'Optimizador'
        db.create_table(u'coordinador_optimizador', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('porcentaje_efectividad', self.gf('django.db.models.fields.IntegerField')()),
            ('puestos', self.gf('django.db.models.fields.IntegerField')()),
            ('programa', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['coordinador.Programa'])),
        ))
        db.send_create_signal(u'coordinador', ['Optimizador'])

        # Adding model 'Coordinador'
        db.create_table(u'coordinador_coordinador', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('estado', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'coordinador', ['Coordinador'])

        # Adding model 'ConfiguracionCursoTempo'
        db.create_table(u'coordinador_configuracioncursotempo', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('semestre', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['coordinador.Semestre'])),
            ('numero_estudiantes', self.gf('django.db.models.fields.IntegerField')()),
            ('curso', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['coordinador.Curso'])),
            ('total_inscritos', self.gf('django.db.models.fields.IntegerField')()),
            ('optimizador', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['coordinador.Optimizador'])),
        ))
        db.send_create_signal(u'coordinador', ['ConfiguracionCursoTempo'])

        # Adding model 'PorcentajeProgramaTempo'
        db.create_table(u'coordinador_porcentajeprogramatempo', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('programa', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['coordinador.Programa'])),
            ('porcentaje', self.gf('django.db.models.fields.IntegerField')()),
            ('configuracion_curso', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['coordinador.ConfiguracionCursoTempo'])),
        ))
        db.send_create_signal(u'coordinador', ['PorcentajeProgramaTempo'])

        # Adding field 'Programa.coordinador'
        db.add_column(u'coordinador_programa', 'coordinador',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['coordinador.Coordinador'], null=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting model 'PorcentajePrograma'
        db.delete_table(u'coordinador_porcentajeprograma')

        # Deleting model 'ConfiguracionCurso'
        db.delete_table(u'coordinador_configuracioncurso')

        # Deleting model 'Optimizador'
        db.delete_table(u'coordinador_optimizador')

        # Deleting model 'Coordinador'
        db.delete_table(u'coordinador_coordinador')

        # Deleting model 'ConfiguracionCursoTempo'
        db.delete_table(u'coordinador_configuracioncursotempo')

        # Deleting model 'PorcentajeProgramaTempo'
        db.delete_table(u'coordinador_porcentajeprogramatempo')

        # Deleting field 'Programa.coordinador'
        db.delete_column(u'coordinador_programa', 'coordinador_id')


    models = {
        u'coordinador.configuracioncurso': {
            'Meta': {'ordering': "('curso',)", 'object_name': 'ConfiguracionCurso'},
            'curso': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.Curso']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'numero_estudiantes': ('django.db.models.fields.IntegerField', [], {}),
            'semestre': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.Semestre']"})
        },
        u'coordinador.configuracioncursotempo': {
            'Meta': {'ordering': "('semestre',)", 'object_name': 'ConfiguracionCursoTempo'},
            'curso': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.Curso']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'numero_estudiantes': ('django.db.models.fields.IntegerField', [], {}),
            'optimizador': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.Optimizador']"}),
            'semestre': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.Semestre']"}),
            'total_inscritos': ('django.db.models.fields.IntegerField', [], {})
        },
        u'coordinador.coordinador': {
            'Meta': {'ordering': "('nombre',)", 'object_name': 'Coordinador'},
            'estado': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        u'coordinador.curso': {
            'Meta': {'ordering': "('nombre',)", 'object_name': 'Curso'},
            'codigo': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'creditos': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'creditos_minimos': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'es_electiva': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'estado': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'estudiantes': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['coordinador.Estudiante']", 'through': u"orm['coordinador.RegistroCurso']", 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'programa': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.Programa']"})
        },
        u'coordinador.estudiante': {
            'Meta': {'ordering': "('nombre',)", 'object_name': 'Estudiante'},
            'edad': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'estado': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'foto': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        u'coordinador.optimizador': {
            'Meta': {'ordering': "('programa',)", 'object_name': 'Optimizador'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'porcentaje_efectividad': ('django.db.models.fields.IntegerField', [], {}),
            'programa': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.Programa']"}),
            'puestos': ('django.db.models.fields.IntegerField', [], {})
        },
        u'coordinador.planeadorestudio': {
            'Meta': {'ordering': "('nombre',)", 'object_name': 'PlaneadorEstudio'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'num_sem_plan': ('django.db.models.fields.IntegerField', [], {'default': '4'})
        },
        u'coordinador.porcentajeprograma': {
            'Meta': {'ordering': "('programa',)", 'object_name': 'PorcentajePrograma'},
            'configuracion_curso': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.ConfiguracionCurso']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'porcentaje': ('django.db.models.fields.IntegerField', [], {}),
            'programa': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.Programa']"})
        },
        u'coordinador.porcentajeprogramatempo': {
            'Meta': {'ordering': "('programa',)", 'object_name': 'PorcentajeProgramaTempo'},
            'configuracion_curso': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.ConfiguracionCursoTempo']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'porcentaje': ('django.db.models.fields.IntegerField', [], {}),
            'programa': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.Programa']"})
        },
        u'coordinador.programa': {
            'Meta': {'ordering': "('nombre',)", 'object_name': 'Programa'},
            'codigo': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'coordinador': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.Coordinador']", 'null': 'True'}),
            'creditos_totales': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'estado': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'estudiantes': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['coordinador.Estudiante']", 'through': u"orm['coordinador.ProgramaEstudiante']", 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'planeador_estudio': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.PlaneadorEstudio']"}),
            'tipo_programa': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.TipoPrograma']"})
        },
        u'coordinador.programaestudiante': {
            'Meta': {'ordering': "('programa',)", 'object_name': 'ProgramaEstudiante'},
            'actual': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'estudiante': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.Estudiante']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'programa': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.Programa']"})
        },
        u'coordinador.registrocurso': {
            'Meta': {'ordering': "('curso',)", 'object_name': 'RegistroCurso'},
            'curso': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.Curso']"}),
            'estudiante': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.Estudiante']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'semestre': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.Semestre']"}),
            'tipo_registro': ('django.db.models.fields.related.ForeignKey', [], {'default': '3', 'to': u"orm['coordinador.TipoRegistroCurso']"})
        },
        u'coordinador.semestre': {
            'Meta': {'ordering': "('nombre',)", 'object_name': 'Semestre'},
            'estado': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'fecha_inicial': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '7'}),
            'planeador_estudio': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.PlaneadorEstudio']"})
        },
        u'coordinador.tipoprograma': {
            'Meta': {'ordering': "('nombre',)", 'object_name': 'TipoPrograma'},
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'estado': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'coordinador.tiporegistrocurso': {
            'Meta': {'ordering': "('nombre',)", 'object_name': 'TipoRegistroCurso'},
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'estado': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['coordinador']