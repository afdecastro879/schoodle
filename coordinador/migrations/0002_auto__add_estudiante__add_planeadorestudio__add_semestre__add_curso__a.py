# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Estudiante'
        db.create_table(u'coordinador_estudiante', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=500)),
            ('edad', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('telefono', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('foto', self.gf('django.db.models.fields.CharField')(max_length=500)),
            ('estado', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'coordinador', ['Estudiante'])

        # Adding model 'PlaneadorEstudio'
        db.create_table(u'coordinador_planeadorestudio', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('num_sem_plan', self.gf('django.db.models.fields.IntegerField')(default=4)),
        ))
        db.send_create_signal(u'coordinador', ['PlaneadorEstudio'])

        # Adding model 'Semestre'
        db.create_table(u'coordinador_semestre', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=7)),
            ('estado', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'coordinador', ['Semestre'])

        # Adding model 'Curso'
        db.create_table(u'coordinador_curso', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('codigo', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('creditos', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('descripcion', self.gf('django.db.models.fields.CharField')(max_length=500)),
            ('estado', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('es_electiva', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'coordinador', ['Curso'])

        # Adding model 'RegistroCurso'
        db.create_table(u'coordinador_registrocurso', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('cursado', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('aprobado', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('semestre', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['coordinador.Semestre'])),
            ('curso', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['coordinador.Curso'])),
            ('estudiante', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['coordinador.Estudiante'])),
        ))
        db.send_create_signal(u'coordinador', ['RegistroCurso'])

        # Adding model 'Programa'
        db.create_table(u'coordinador_programa', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('programa', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['coordinador.PlaneadorEstudio'])),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('descripcion', self.gf('django.db.models.fields.CharField')(max_length=500)),
            ('codigo', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('tipo_programa', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['coordinador.TipoPrograma'])),
            ('estado', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'coordinador', ['Programa'])

        # Adding model 'ProgramaEstudiante'
        db.create_table(u'coordinador_programaestudiante', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('actual', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('programa', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['coordinador.Programa'])),
            ('estudiante', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['coordinador.Estudiante'])),
        ))
        db.send_create_signal(u'coordinador', ['ProgramaEstudiante'])

        # Adding model 'TipoPrograma'
        db.create_table(u'coordinador_tipoprograma', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('descripcion', self.gf('django.db.models.fields.CharField')(max_length=500)),
            ('estado', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'coordinador', ['TipoPrograma'])


    def backwards(self, orm):
        # Deleting model 'Estudiante'
        db.delete_table(u'coordinador_estudiante')

        # Deleting model 'PlaneadorEstudio'
        db.delete_table(u'coordinador_planeadorestudio')

        # Deleting model 'Semestre'
        db.delete_table(u'coordinador_semestre')

        # Deleting model 'Curso'
        db.delete_table(u'coordinador_curso')

        # Deleting model 'RegistroCurso'
        db.delete_table(u'coordinador_registrocurso')

        # Deleting model 'Programa'
        db.delete_table(u'coordinador_programa')

        # Deleting model 'ProgramaEstudiante'
        db.delete_table(u'coordinador_programaestudiante')

        # Deleting model 'TipoPrograma'
        db.delete_table(u'coordinador_tipoprograma')


    models = {
        u'coordinador.curso': {
            'Meta': {'ordering': "('nombre',)", 'object_name': 'Curso'},
            'codigo': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'creditos': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'es_electiva': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'estado': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'estudiantes': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['coordinador.Estudiante']", 'through': u"orm['coordinador.RegistroCurso']", 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'coordinador.estudiante': {
            'Meta': {'ordering': "('nombre',)", 'object_name': 'Estudiante'},
            'edad': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'estado': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'foto': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        u'coordinador.planeadorestudio': {
            'Meta': {'ordering': "('nombre',)", 'object_name': 'PlaneadorEstudio'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'num_sem_plan': ('django.db.models.fields.IntegerField', [], {'default': '4'})
        },
        u'coordinador.programa': {
            'Meta': {'ordering': "('nombre',)", 'object_name': 'Programa'},
            'codigo': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'estado': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'estudiantes': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['coordinador.Estudiante']", 'through': u"orm['coordinador.ProgramaEstudiante']", 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'programa': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.PlaneadorEstudio']"}),
            'tipo_programa': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.TipoPrograma']"})
        },
        u'coordinador.programaestudiante': {
            'Meta': {'ordering': "('programa',)", 'object_name': 'ProgramaEstudiante'},
            'actual': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'estudiante': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.Estudiante']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'programa': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.Programa']"})
        },
        u'coordinador.registrocurso': {
            'Meta': {'ordering': "('curso',)", 'object_name': 'RegistroCurso'},
            'aprobado': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'cursado': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'curso': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.Curso']"}),
            'estudiante': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.Estudiante']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'semestre': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.Semestre']"})
        },
        u'coordinador.semestre': {
            'Meta': {'ordering': "('nombre',)", 'object_name': 'Semestre'},
            'estado': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '7'})
        },
        u'coordinador.tipoprograma': {
            'Meta': {'ordering': "('nombre',)", 'object_name': 'TipoPrograma'},
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'estado': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['coordinador']