# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import DataMigration
from django.db import models

class Migration(DataMigration):

    def forwards(self, orm):
        "Write your forwards methods here."
        from django.core.management import call_command
        call_command("loaddata", "load_cursos.json")
        #call_command("loaddata", "load_registrocursos.json")
        # Note: Don't use "from appname.models import ModelName".
        # Use orm.ModelName to refer to models in this application,
        # and orm['appname.ModelName'] for models in other applications.

    def backwards(self, orm):
        "Write your backwards methods here."

    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'coordinador.configuracioncurso': {
            'Meta': {'ordering': "('curso',)", 'object_name': 'ConfiguracionCurso'},
            'curso': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.Curso']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'numero_estudiantes': ('django.db.models.fields.IntegerField', [], {}),
            'semestre': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.Semestre']"})
        },
        u'coordinador.configuracioncursotempo': {
            'Meta': {'ordering': "('semestre',)", 'object_name': 'ConfiguracionCursoTempo'},
            'curso': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.Curso']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'numero_estudiantes': ('django.db.models.fields.IntegerField', [], {}),
            'optimizador': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.Optimizador']"}),
            'semestre': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.Semestre']"}),
            'total_inscritos': ('django.db.models.fields.IntegerField', [], {})
        },
        u'coordinador.coordinador': {
            'Meta': {'ordering': "('nombre',)", 'object_name': 'Coordinador'},
            'estado': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'usuario': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True'})
        },
        u'coordinador.curso': {
            'Meta': {'ordering': "('nombre',)", 'object_name': 'Curso'},
            'codigo': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'creditos': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'creditos_minimos': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'es_electiva': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'estado': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'estudiantes': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['coordinador.Estudiante']", 'through': u"orm['coordinador.RegistroCurso']", 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'programa': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.Programa']"})
        },
        u'coordinador.estudiante': {
            'Meta': {'ordering': "('nombre',)", 'object_name': 'Estudiante'},
            'edad': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'estado': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'foto': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'usuario': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True'})
        },
        u'coordinador.optimizador': {
            'Meta': {'ordering': "('programa',)", 'object_name': 'Optimizador'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'porcentaje_efectividad': ('django.db.models.fields.IntegerField', [], {}),
            'programa': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.Programa']"}),
            'puestos': ('django.db.models.fields.IntegerField', [], {})
        },
        u'coordinador.planeadorestudio': {
            'Meta': {'ordering': "('nombre',)", 'object_name': 'PlaneadorEstudio'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'num_sem_plan': ('django.db.models.fields.IntegerField', [], {'default': '4'})
        },
        u'coordinador.porcentajeprograma': {
            'Meta': {'ordering': "('programa',)", 'object_name': 'PorcentajePrograma'},
            'configuracion_curso': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.ConfiguracionCurso']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'porcentaje': ('django.db.models.fields.IntegerField', [], {}),
            'programa': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.Programa']"})
        },
        u'coordinador.porcentajeprogramatempo': {
            'Meta': {'ordering': "('programa',)", 'object_name': 'PorcentajeProgramaTempo'},
            'configuracion_curso': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.ConfiguracionCursoTempo']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'porcentaje': ('django.db.models.fields.IntegerField', [], {}),
            'programa': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.Programa']"})
        },
        u'coordinador.programa': {
            'Meta': {'ordering': "('nombre',)", 'object_name': 'Programa'},
            'codigo': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'coordinador': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.Coordinador']", 'null': 'True'}),
            'creditos_totales': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'estado': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'estudiantes': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['coordinador.Estudiante']", 'through': u"orm['coordinador.ProgramaEstudiante']", 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'planeador_estudio': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.PlaneadorEstudio']"}),
            'tipo_programa': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.TipoPrograma']"})
        },
        u'coordinador.programaestudiante': {
            'Meta': {'ordering': "('programa',)", 'object_name': 'ProgramaEstudiante'},
            'actual': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'estudiante': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.Estudiante']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'programa': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.Programa']"})
        },
        u'coordinador.registrocurso': {
            'Meta': {'ordering': "('curso',)", 'object_name': 'RegistroCurso'},
            'curso': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.Curso']"}),
            'estudiante': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.Estudiante']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'semestre': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.Semestre']"}),
            'tipo_registro': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.TipoRegistroCurso']"})
        },
        u'coordinador.semestre': {
            'Meta': {'ordering': "('nombre',)", 'object_name': 'Semestre'},
            'estado': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'fecha_inicial': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '7'}),
            'planeador_estudio': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coordinador.PlaneadorEstudio']"})
        },
        u'coordinador.tipoprograma': {
            'Meta': {'ordering': "('nombre',)", 'object_name': 'TipoPrograma'},
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'estado': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'coordinador.tiporegistrocurso': {
            'Meta': {'ordering': "('nombre',)", 'object_name': 'TipoRegistroCurso'},
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'estado': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['coordinador']
    symmetrical = True
