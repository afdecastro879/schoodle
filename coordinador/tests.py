# coding=utf-8
from django.test import TestCase
from coordinador.models import *
from datetime import datetime
# Create your tests here..


class PlaneadorEstudioTestCase(TestCase):
    def setUp(self):
        self.plan1=PlaneadorEstudio.objects.create(nombre="Plan1", num_sem_plan=3)
        self.plan2=PlaneadorEstudio.objects.create(nombre="Plan2", num_sem_plan=2)

        self.semes=Semestre.objects.create(nombre="2014-01", planeador_estudio=self.plan1, estado=True, fecha_inicial=datetime.now())
        self.semes2=Semestre.objects.create(nombre="2014-02", planeador_estudio=self.plan2, estado=True, fecha_inicial=datetime.now())

        self.tipoprograma=TipoPrograma.objects.create(nombre="tipo1", descripcion="desc1", estado=True)
        self.tipoprograma2=TipoPrograma.objects.create(nombre="tipo2", descripcion="desc2", estado=False)

        self.programa1=Programa.objects.create(planeador_estudio=self.plan1, nombre="MESI", descripcion="desc1", codigo="MESI", tipo_programa=self.tipoprograma, estado=True)
        self.programa2=Programa.objects.create(planeador_estudio=self.plan2, nombre="MISO", descripcion="desc2", codigo="MISO", tipo_programa=self.tipoprograma2, estado=True)

        self.curso1=Curso.objects.create(nombre="curso1", codigo="4101", creditos=4, descripcion="desc1", programa=self.programa1, estado=True, es_electiva=True)
        self.curso2=Curso.objects.create(nombre="curso2", codigo="4202", creditos=4, descripcion="desc2", programa=self.programa2, estado=True, es_electiva=False)

        self.estudiante1=Estudiante.objects.create(usuario=1, edad="19", telefono=41231, foto="foto1", estado=True)
        self.estudiante2=Estudiante.objects.create(usuario=2, edad="20", telefono=123456, foto="foto2", estado=True)

        ProgramaEstudiante.objects.create(actual=True, programa=self.programa1, estudiante=self.estudiante1)
        ProgramaEstudiante.objects.create(actual=True, programa=self.programa2, estudiante=self.estudiante2)

    def testSemestre(self):
        self.assertEqual(self.semes.nombre, '2014-01')
        self.assertEqual(self.semes2.nombre, '2014-02')
        self.assertEqual(len(Semestre.objects.all()), 2)
        self.semes2.delete()
        self.assertEqual(len(Semestre.objects.all()), 1)
        self.semes2.nombre="TestUpdate"
        self.assertEqual(self.semes2.nombre, 'TestUpdate')


    def testPlaneador(self):
        self.assertEqual(self.plan1.num_sem_plan, 3)
        self.assertEqual(self.plan2.num_sem_plan, 2)
        self.assertEqual(len(PlaneadorEstudio.objects.all()), 2)
        self.plan1.delete()
        self.assertEqual(len(PlaneadorEstudio.objects.all()), 1)
        self.plan2.nombre="TestUpdate"
        self.assertEqual(self.plan2.nombre, 'TestUpdate')

    def testTipoPrograma(self):
        self.assertEqual(self.tipoprograma.descripcion, 'desc1')
        self.assertEqual(self.tipoprograma2.estado, False)
        self.assertEqual(len(TipoPrograma.objects.all()), 2)
        self.tipoprograma2.delete()
        self.assertEqual(len(TipoPrograma.objects.all()), 1)
        self.tipoprograma.nombre="TestUpdate"
        self.assertEqual(self.tipoprograma.nombre, 'TestUpdate')


    def testPrograma(self):
        self.assertEqual(self.programa1.nombre, 'MESI')
        self.assertEqual(self.programa2.nombre, 'MISO')
        self.assertEqual(len(Programa.objects.all()), 2)
        self.programa1.delete()
        self.assertEqual(len(Programa.objects.all()), 1)
        self.programa2.nombre="TestUpdate"
        self.assertEqual(self.programa2.nombre, 'TestUpdate')


    def testCurso(self):
        self.assertEqual(self.curso1.programa.nombre, 'MESI')
        self.assertEqual(self.curso2.codigo, '4202')
        self.assertEqual(len(Curso.objects.all()), 2)
        self.curso1.delete()
        self.assertEqual(len(Curso.objects.all()), 1)
        self.curso2.nombre="TestUpdate"
        self.assertEqual(self.curso2.nombre, 'TestUpdate')

    def testProgramaEstudiante(self):
        self.assertEqual(len(ProgramaEstudiante.objects.all()), 2)
        self.assertEqual(ProgramaEstudiante.objects.get(programa=self.programa2).estudiante, self.estudiante2)

