# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.db import models

# Create your models here.

class PlaneadorEstudio(models.Model):
    nombre = models.CharField(max_length=200)
    num_sem_plan = models.IntegerField(default=4)

    def __unicode__(self):
        return u'%s' % self.nombre

    class Meta:
        ordering = ('nombre',)


class TipoPrograma(models.Model):
    TIPOS = (
        ('MAESTRIA', 'MAESTRIA'),
        ('PREGRADO', 'PREGRADO'),
        ('ESPECIALIZACION', 'ESPECIALIZACION'),
    )
    nombre = models.CharField(max_length=200, choices=TIPOS)
    descripcion = models.CharField(max_length=500, verbose_name="Descripción")
    estado = models.BooleanField(default=True)

    def __unicode__(self):
        return u'%s' % self.nombre

    class Meta:
        ordering = ('nombre',)


class Semestre(models.Model):
    SEMESTRES = (
        ('2014-10', '2014-10'),
        ('2014-18', '2014-18'),
        ('2014-20', '2014-20'),
        ('2015-10', '2015-10'),
        ('2015-18', '2015-18'),
        ('2015-20', '2015-20'),
        ('2016-10', '2016-10'),
        ('2016-18', '2016-18'),
        ('2016-20', '2016-20'),
        ('2017-10', '2017-10')
    )
    nombre = models.CharField(max_length=7, choices=SEMESTRES)
    planeador_estudio = models.ForeignKey(PlaneadorEstudio)
    fecha_inicial = models.DateField()
    estado = models.BooleanField(default=True)

    def __unicode__(self):
        return u'%s' % self.nombre

    class Meta:
        ordering = ('nombre',)

    def json(self):
        return {
            'id': self.id,
            'nombre': self.nombre,
            'estado': self.estado,
            'planeador_estudio': self.planeador_estudio.id,
        }


class Estudiante(models.Model):
    usuario = models.OneToOneField(User)
    codigo = models.CharField(max_length=500, null=True, verbose_name="Código")
    edad = models.IntegerField(default=0)
    telefono = models.CharField(max_length=20, verbose_name="Teléfono")
    foto = models.CharField(max_length=500)
    estado = models.BooleanField(default=True)

    def __unicode__(self):
        return u'%s %s' % (self.usuario.first_name, self.usuario.last_name)

    class Meta:
        ordering = ('usuario__last_name', 'usuario__first_name',)

    def promedio(self):
        registrosAprobados = RegistroCurso.objects.filter(estudiante=self.pk, tipo_registro__nombre='APROBADO').all()
        promedio = 0
        for reg in registrosAprobados:
            promedio = promedio + reg.curso.creditos
        return promedio

    def programa(self):
        registrosProgramas = ProgramaEstudiante.objects.filter(estudiante=self.pk, actual=True).all()
        return registrosProgramas[0].programa.descripcion

        # clase nueva coordinador

    @staticmethod
    def autorizacion(user_id):
        user = User.objects.get(pk=user_id)
        try:
            user.estudiante
            return 'estudiante'
        except ObjectDoesNotExist:
            try:
                user.coordinador
                return 'coordinador'
            except ObjectDoesNotExist:
                return 'admin'

    def json_plan(self):

        import time

        current_year = time.strftime("%Y")
        current_month = time.strftime("%m")
        if current_month <= 4:
            future_semesters = [current_year + "-18", current_year + "-20",
                                str(int(current_year) + 1) + "-10", str(int(current_year) + 1) + "-18"]
        elif current_month in [6, 5]:
            future_semesters = [current_year + "-20", str(int(current_year) + 1) + "-01",
                                str(int(current_year) + 1) + "-18", str(int(current_year) + 1) + "-20"]
        else:
            future_semesters = [str(int(current_year) + 1) + "-10", str(int(current_year) + 1) + "-18",
                                str(int(current_year) + 1) + "-20", str(int(current_year) + 2) + "-10"]

        list_semesters = Semestre.objects.filter(planeador_estudio=1, estado=True,
                                                 nombre__in=future_semesters).all().order_by('fecha_inicial')
        list_registers = RegistroCurso.objects.filter(estudiante=self.pk, semestre__in=list_semesters,
                                                      tipo_registro="3").all().order_by('semestre__fecha_inicial')
        # Creditos AProbados
        list_registro_curso_user = RegistroCurso.objects.filter(tipo_registro=1, estudiante=self.id).exclude(
            semestre__in=list_semesters)
        creditos_aprobados_curso_user = 0
        for cursos in list_registro_curso_user:
            creditos_aprobados_curso_user += cursos.curso.creditos
        # print list_registers
        # creditos presentes en el planeador
        creditos_totales_planeador = 0
        plan_student = []
        creditos_necesarios = creditos_aprobados_curso_user
        for semester in list_semesters:
            list_temp = []
            creditos_periodo = 0
            for register in list_registers:
                # print register
                if register.semestre == semester:
                    cred_min = register.curso.creditos_minimos - creditos_necesarios
                    if cred_min < 0: cred_min = 0
                    list_temp.append({'curso': register.curso.json(), 'id': register.id, 'creditos_minimos': cred_min})
                    creditos_totales_planeador += register.curso.creditos
                    creditos_periodo += register.curso.creditos

            creditos_necesarios += creditos_periodo
            plan_semester = {'name': semester.nombre, 'data': list_temp, 'id': semester.id}
            plan_student.append(plan_semester)

        # Cargamos las maestrias
        tipo_maestria = TipoPrograma.objects.get(nombre='MAESTRIA')
        prog_est = ProgramaEstudiante.objects.filter(actual=1).get(estudiante=self.id)
        # lista_maestrias = Programa.objects.filter(tipo_programa=tipo_maestria).exclude(pk=prog_est.programa.pk)


        #Creditos faltantes
        creditos_faltantes_Programa = prog_est.programa.creditos_totales
        creditos_faltantes = creditos_faltantes_Programa - creditos_aprobados_curso_user
        #Cargamos la maestria del estudiante

        data = {'plan_student': plan_student,
                'creditos_aprobados_curso_user': creditos_aprobados_curso_user,
                'creditos_totales_planeador': creditos_totales_planeador, 'creditos_faltantes': creditos_faltantes}
        print data
        return data

    def getCursosSinPlanear(self, idPrograma):
        cursos_sin_planear = Curso.objects.select_related('RegistroCurso').filter(programa__id=idPrograma).exclude(
            registrocurso__estudiante__id=self.id, registrocurso__tipo_registro__nombre='PLANEADO').exclude(
            registrocurso__estudiante__id=self.id, registrocurso__tipo_registro__nombre='APROBADO')
        data = [curso.json() for curso in cursos_sin_planear]
        return data


class Coordinador(models.Model):
    usuario = models.OneToOneField(User)
    estado = models.BooleanField(default=True)

    def __unicode__(self):
        return u'%s %s' % (self.usuario.first_name, self.usuario.last_name)

    class Meta:
        ordering = ('usuario__first_name',)
        verbose_name_plural = 'Coordinadores'


class Programa(models.Model):
    coordinador = models.ForeignKey(Coordinador, null=True)
    planeador_estudio = models.ForeignKey(PlaneadorEstudio)
    nombre = models.CharField(max_length=200)
    descripcion = models.CharField(max_length=500, verbose_name="Descripción")
    codigo = models.CharField(max_length=50, verbose_name="Código")
    tipo_programa = models.ForeignKey(TipoPrograma)
    creditos_totales = models.IntegerField(default=0, verbose_name="Créditos Totales")
    estado = models.BooleanField(default=True)
    estudiantes = models.ManyToManyField(Estudiante, through='ProgramaEstudiante')

    def __unicode__(self):
        return u'%s' % self.nombre

    class Meta:
        ordering = ('nombre',)


    def listaEstudiantePorPrograma(self):
        estudiantes = self.estudiantes.all()
        return estudiantes


class Curso(models.Model):
    nombre = models.CharField(max_length=200)
    codigo = models.CharField(max_length=50, verbose_name="Código")
    creditos = models.IntegerField(default=0, verbose_name="Créditos")
    descripcion = models.CharField(max_length=500, verbose_name="Descripción")
    programa = models.ForeignKey(Programa)
    creditos_minimos = models.IntegerField(default=0, verbose_name="Créditos Mínimos")
    estado = models.BooleanField(default=True)
    es_electiva = models.BooleanField(default=True)
    estudiantes = models.ManyToManyField(Estudiante, through='RegistroCurso')

    def __unicode__(self):
        return u'%s' % self.nombre

    @staticmethod
    def darMateriasVistas(idEstudiante):
        registrosAprobados = RegistroCurso.objects.filter(estudiante=idEstudiante,
                                                          tipo_registro__nombre='APROBADO').all()
        # cursosaprobados = 0
        #for reg in registrosAprobados:
        #    cursosaprobados = cursosaprobados + reg.curso
        return registrosAprobados

    @staticmethod
    def darMateriasCursando(idEstudiante):
        materiasCursando = RegistroCurso.objects.filter(estudiante=idEstudiante, tipo_registro__nombre='EN CURSO').all()
        return materiasCursando

    @staticmethod
    def darMateriasPlaneadas(idEstudiante):
        materiasPlaneadas = RegistroCurso.objects.filter(estudiante=idEstudiante,
                                                         tipo_registro__nombre='PLANEADO').all()
        return materiasPlaneadas

    class Meta:
        ordering = ('nombre',)

    def json(self):
        return {
            'nombre': self.nombre,
            'codigo': self.codigo,
            'creditos': self.creditos,
            'descripcion': self.descripcion,
            'programa': self.programa.nombre,
            'creditos_minimos': self.creditos_minimos,
            'id': self.id,
        }


class ProgramaEstudiante(models.Model):
    actual = models.BooleanField(default=True)
    programa = models.ForeignKey(Programa)
    estudiante = models.ForeignKey(Estudiante)

    class Meta:
        ordering = ('programa',)


class TipoRegistroCurso(models.Model):
    TIPOS = (
        ('APROBADO', 'APROBADO'),
        ('CURSADO', 'CURSADO'),
        ('PLANEADO', 'PLANEADO'),
        ('EN CURSO', 'EN CURSO'),
    )
    nombre = models.CharField(max_length=200, choices=TIPOS)
    descripcion = models.CharField(max_length=500, verbose_name="Descripción")
    estado = models.BooleanField(default=True)

    def __unicode__(self):
        return u'%s' % self.nombre

    class Meta:
        ordering = ('nombre',)


class RegistroCurso(models.Model):
    semestre = models.ForeignKey(Semestre)
    curso = models.ForeignKey(Curso)
    estudiante = models.ForeignKey(Estudiante)
    tipo_registro = models.ForeignKey(TipoRegistroCurso)
    # nota = models.IntegerField(default=0, verbose_name="Nota")

    def __unicode__(self):
        return u'%s' % self.curso.nombre

    class Meta:
        ordering = ('curso',)


    def json(self):
        return {
            'periodo': self.semestre.nombre,
            'creditos': self.curso.creditos,
            'nota': 5,
            'curso': self.curso.nombre,
        }

        # nuevas clases


class ConfiguracionCurso(models.Model):
    semestre = models.ForeignKey(Semestre)
    numero_estudiantes = models.IntegerField()
    curso = models.ForeignKey(Curso)

    def __unicode__(self):
        return u'%s' % self.curso.nombre

    class Meta:
        ordering = ('curso',)


class PorcentajePrograma(models.Model):
    programa = models.ForeignKey(Programa)
    porcentaje = models.IntegerField()
    configuracion_curso = models.ForeignKey(ConfiguracionCurso)

    class Meta:
        ordering = ('programa',)


class Optimizador(models.Model):
    NIVEL_CREDITOS = (
        ('0-8', '0-8'),
        ('9-16', '9-16'),
        ('16-24', '16-24'),
        ('24-32', '24-32'),
        ('32-40', '32-40')
    )
    porcentaje_efectividad = models.IntegerField()
    puestos = models.IntegerField()
    programa = models.ForeignKey(Programa)

    class Meta:
        ordering = ('programa',)


class ConfiguracionCursoTempo(models.Model):
    semestre = models.ForeignKey(Semestre)
    numero_estudiantes = models.IntegerField()
    curso = models.ForeignKey(Curso)
    total_inscritos = models.IntegerField()
    optimizador = models.ForeignKey(Optimizador)

    class Meta:
        ordering = ('semestre',)


class PorcentajeProgramaTempo(models.Model):
    programa = models.ForeignKey(Programa)
    porcentaje = models.IntegerField()
    configuracion_curso = models.ForeignKey(ConfiguracionCursoTempo)

    class Meta:
        ordering = ('programa',)