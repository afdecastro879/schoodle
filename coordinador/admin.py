from django.contrib import admin
from coordinador.models import *

class CourseInline(admin.TabularInline):
    model = Curso
    extra = 3


class ProgramaEstudianteInline(admin.TabularInline):
    fields = ['programa', 'estudiante', 'actual']
    model = ProgramaEstudiante
    extra = 3


class SemestresInline(admin.TabularInline):
    model = Semestre
    extra = 3


class ProgramaAdmin(admin.ModelAdmin):
    search_fields = ['nombre']
    inlines = [CourseInline, ProgramaEstudianteInline]


class PlaneadorAdmin(admin.ModelAdmin):
    inlines = [SemestresInline]


class RegistroCursoAdmin(admin.ModelAdmin):
    fields = ['curso', 'semestre', 'estudiante', 'tipo_registro']
    list_filter = ['estudiante__usuario__first_name', 'tipo_registro__nombre', 'curso__nombre']


class EstudianteAdmin(admin.ModelAdmin):
    search_fields = ['usuario__first_name']
    inlines = [ProgramaEstudianteInline]

admin.site.register(PlaneadorEstudio, PlaneadorAdmin)
admin.site.register(Programa, ProgramaAdmin)
#admin.site.register(Programa)
#admin.site.register(Curso)
admin.site.register(TipoPrograma)
#admin.site.register(Semestre)
admin.site.register(Estudiante, EstudianteAdmin)
admin.site.register(RegistroCurso, RegistroCursoAdmin)
#admin.site.register(ProgramaEstudiante)
admin.site.register(Coordinador)