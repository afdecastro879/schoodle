var deleting = false;
function masterList() {
    $(".list-group-item").click(function (e) {
        e.preventDefault();
        var index = $(this).index();
        $(".list-group-item").removeClass("active");
        //$(".list-group-item").eq(index).addClass("active");
        //$(".list-group-item").find('h2').replaceWith(function() {
        //      return '<h4 class="glyphicon glyphicon-book">' + $(this).text() + '</h4>';
        //});
        $(this).addClass("active");
        //$(this).find('h4').replaceWith(function() {
        //       return '<h2 class="glyphicon glyphicon-book" style = "margin:auto">' + $(this).text() + '</h2>';
        //});


    });
}

function loadInfoMaterias() {
    $(document).off('click', '.eliminarMateriaPlaneador');
    $(document).off('click', '.info-materias');
    $(".eliminarMateriaPlaneador").click(function (e) {
        var idRegistroCurso = ($(this).data("id"));
        if (idRegistroCurso && idRegistroCurso > 0) {
            eliminarMateria(idRegistroCurso, $(this));
        }
    });
    $(".info-materias").click(function (e) {
        if(deleting)return;
        e.preventDefault();
        var id = $(this).attr("data-curso")
        $.getJSON("infoMateria/"+id, function (data) {
            bootbox.dialog({
                title: "Información de la Asignatura",
                message: "<div style='text-aling: center'>" +
                    "<p>" + data.nombre + "</p>" +
                    "<ul>" +
                    "<li>Programa: " + data.programa + "</li>" +
                    "<li>Código: " + data.codigo + "</li>" +
                    "<li>Créditos: " + data.creditos + "</li>" +
                    "<li>Descripción: " + data.descripcion + "</li>" +
                    "<li>Créditos Mínimos para asegurar cupo: " + data.creditos_minimos + "</li>" +
                    "</ul>" +
                    "</div>",
                buttons: {
                    main: {
                        label: "Aceptar",
                        className: "btn-primary",
                        callback: function () {

                        }
                    }
                }
            });

        });
    });
}

function cargarDraggable() {
    $(".draggable").draggable({
        containment: '.contenido',
        cursor: 'move',
        stack: '.materias div',
        revert: true
    });
    $('.droppable').droppable({
        accept: '.materias div',
        hoverClass: 'hovered',
        drop: handleDropEvent
    });
}

function handleDropEvent(event, ui) {
    var draggable = ui.draggable;
    //Traer el id del semestre al que llegó la materia
    var idPlan = $(this).attr("data-plan").split('-')[1];
    //Traer el id de la materia draggueada
    var id = draggable.attr('id');
    var thisElement = $(this);
    var draggableHtml = draggable.html();
    thisElement.html(draggableHtml);
    draggable.remove();
    //ui.draggable.draggable('option', 'revert', false);
    $.post('/agregarRegistro/' + id + '-' + idPlan, {csrfmiddlewaretoken: token}, function (res) {
        if (res.response = 'success') {
            updatePlaneador(res.data);
            //ui.draggable.draggable('disable');
            //thisElement.droppable('disable');
            //thisElement.addClass('plan_item');
            //thisElement.append("<a onclick='eliminarMateria("+res.id+", $(this) )' data-id='"+id+"' href='javascript:void(0)' ><span style='bottom:0;right:0;position:absolute;color:black' class='glyphicon glyphicon-remove'></span></a>");
            //ui.draggable.position({ of: thisElement, my: 'left top', at: 'left top' });
        } else {
            ui.draggable.draggable('option', 'revert', true);
        }
    });
}
function updatePlaneador(data){
      $('#planeador').html('');
      var content = '';
      for(p in data.plan_student){
            content += '<div class="col-xs-3 semester-box" style="margin-top: 10px">'+
                '<div class="panel panel-default border-blue">'+
                    '<h5 class="center-block text-center"><b>'+data.plan_student[p].name+'</b></h5>'+
                    '<div class="panel-body" style="padding-top: 0; height: 170px;">';
                        var leng = 0;
                        var temp = data.plan_student[p];
                        for(pos in temp.data){
                            leng +=1;
                            content +='<div data-curso="'+temp.data[pos].curso.id+'" id="curso_'+temp.data[pos].curso.id+'" class="info-materias plan_item_container droppable plan_item '+getColorClass(temp.data[pos].creditos_minimos)+'" data-plan="'+leng+'-'+data.plan_student[p].id+'" style="margin-bottom: 10px">'+
                                temp.data[pos].curso.nombre+
                            "<a class='eliminarMateriaPlaneador' data-id='"+temp.data[pos].id+"' href='javascript:void(0)' ><span style='bottom:0;right:0;position:absolute;color:black' class='glyphicon glyphicon-remove'></span></a>" +
                            "</div>";
                        }
                        for(hh=leng+1; hh<=3; hh++) {
                            content +='<div class="plan_item_container droppable" data-plan="'+hh+'-'+data.plan_student[p].id+'" style="margin-bottom: 10px"></div>';
                        }
                    content+='</div></div></div>';
      }
      $('#planeador').html(content);
      $('datos-opt[id=aprobados]').text(data.creditos_aprobados_curso_user);
      $('datos-opt[id=totales]').text(data.creditos_totales_planeador);
      $('datos-opt[id=faltantes]').text(data.creditos_faltantes);
      cargarDraggable();
      loadInfoMaterias()
}

function getColorClass(creditosMinimos){
    if(creditosMinimos <=0)return "green_class"
    else if(creditosMinimos > 0 && creditosMinimos <=8)return "yellow_class"
    else if(creditosMinimos > 8 && creditosMinimos <=16)return "orange_class"
    else return "red_class"
}
function cargarMateriasPorMaestria(materia, csrftoken) {
    var contenedor = $("#materias-cargadas");
    contenedor.html('<div style="margin: 0 auto; width: 10px; padding: 50px; color:#6C6F74"><i class="fa fa-spinner fa-4x fa-spin"></i></div>');
    $.post("/materias/"+materia, {csrfmiddlewaretoken: csrftoken}, function(data){
        contenedor.html("");
        for(var i=0;i<data.length;i++){
            var obj = data[i];
            contenedor.append("<div data-curso='"+obj.id+"' class='draggable plan_item subject info-materias' id='"+obj.id+"'>"+obj.nombre+"</div>");
        }
        cargarDraggable();
        loadInfoMaterias()
    });
}

function eliminarMateria(idRegistroCurso, element){
    deleting = true;
    $.post("deleteRegistro/" + idRegistroCurso, {csrfmiddlewaretoken: token}, function (data) {
        deleting = false;
        if (data.response = 'success') {
            //var mayorPadre = element.parent();
            //mayorPadre.html("");
            //mayorPadre.css({backgroundColor: '', padding: '10px 15px'});
            //element.remove();
            //mayorPadre.droppable();
            //mayorPadre.droppable('enable');
            //mayorPadre.removeClass('plan_item');
            var maestriaActiva = $(".amazingcarousel-image>a.active");
            var idMaestria = maestriaActiva.attr("data-id").split('-')[1];
            //Se recarga el div de las materias incluyendo la materia que se eliminó
            updatePlaneador(data.data);
            cargarMateriasPorMaestria(idMaestria,token);
        }
    });
}